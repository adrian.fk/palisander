import firebase from "../configuration/firebase";


const MerchRepository = () => {



    const merchDbRef = firebase.collection("merch");


    const getMerch = () => {
        return new Promise(
            (resolve) => {
                merchDbRef
                    .get()
                    .then(
                        (item) => {
                            const items = item.docs.map((doc) => doc.data());
                            resolve(items);
                        }
                    )
                    .catch(
                        (err) => console.log(err)
                    )
            }
        );
    }

    const getByIndex = (index) => {
        return new Promise(
            (resolve, reject) => {
                getMerch().then((gallery) => {
                    if (index < gallery.length)
                        resolve(gallery[index])
                    else
                        reject("get Merch with index out of bounds.")
                })
            }
        );
    }

    const getById = (id) => {
        return new Promise(
            (resolve, reject) => {

                merchDbRef
                    .doc(id)
                    .get()
                    .then(
                        (doc) => {
                            if (doc.exists) {
                                resolve(doc.data())
                            }
                            else {
                                reject("No such merch document")
                            }
                        }
                    )
                    .catch((err) => reject(err))
            }
        );
    }

    const deleteById = (id) => {
        return new Promise(
            (resolve, reject) => {

                merchDbRef.doc("" + id).delete()
                    .then(() => {
                        resolve(id);
                    })
                    .catch((err) => {
                        console.log("caught an error: " + err);
                        reject(err, id)
                    });
            }
        );
    }

    const editById = (id, artwork) => {
        return setMerch(id, artwork);
    }

    const setMerch = (id, artwork) => {
        return new Promise(
            (resolve, reject) => {

                merchDbRef.doc("" + id).set({
                    id: id,
                    title: artwork.title,
                    imgPath: artwork.imgPath,
                    thumbnail: artwork.thumbnail,
                    description: artwork.description,
                    price: artwork.price,
                    soldCopies: artwork.soldCopies
                })
                    .then(() => resolve(artwork))
                    .catch((err) => reject(err));
            }
        );
    }

    const addMerch = (merch) => {
        return new Promise(
            (resolve, reject) => {
                merchDbRef
                    .add(merch)
                    .then(
                        (doc) => {
                            merch.id = doc.id;
                            setMerch(doc.id, merch).then((m) => resolve(m));
                        }
                    )
                    .catch((err) => reject(err));
            }
        );
    }

    const onSnapshotById = (id, handlerFunction) => {
        merchDbRef.doc("" + id).onSnapshot(
            (doc) => handlerFunction(doc.data())
        );
    }

    const onMerchSnapshot = (handlerFunction) => {
        merchDbRef.onSnapshot(
            (data) => {
                const items = data.docs.map((doc) => doc.data())
                handlerFunction(items)
            }
        );
    }

    return {
        getMerch,
        getByIndex,
        getById,
        deleteById,
        editById,
        setMerch,
        addMerch,
        onSnapshotById,
        onMerchSnapshot
    }

}

export default MerchRepository;