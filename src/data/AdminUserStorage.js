

export default
class AdminUserStorage {

    constructor() {
        this.user = null;
    }

    static getInstance() {
        if (!AdminUserStorage.INSTANCE) {
            AdminUserStorage.INSTANCE = new AdminUserStorage();
        }
        return AdminUserStorage.INSTANCE;
    }

    setUser(user) {
        this.user = user;
        return this;
    }

    getUser() {
        return this.user;
    }

}