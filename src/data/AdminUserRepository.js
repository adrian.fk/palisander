import firebase from "../configuration/firebase";


function AdminUserRepository() {
    const adminUserDbRef = firebase.collection("AdminUser");

    const getAdminUsers = () => {
        return new Promise(
            (resolve, reject) => {
                adminUserDbRef
                    .get()
                    .then(
                        res => {
                            const items = res.docs.map(doc => doc.data());
                            resolve(items);
                        }
                    )
                    .catch(
                        err => reject(err)
                    );
            }
        );
    }



    return { getAdminUsers }

}

export default AdminUserRepository;

