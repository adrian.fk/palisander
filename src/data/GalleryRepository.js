import firebase from "../configuration/firebase";


const GalleryRepository = () => {

    const artworkDbRef = firebase.collection("artwork");


    const getArtworks = () => {
        return new Promise(
            (resolve) => {
                artworkDbRef
                    .get()
                    .then(
                        (item) => {
                            const items = item.docs.map((doc) => doc.data());
                            resolve(items);
                        }
                    )
            }
        );
    }


    const getGallery = () => {
        return getArtworks();
    }

    const getArtworkByIndex = (index) => {
        //return ImageStubDataPool.get(index);
        return new Promise(
            (resolve, reject) => {
                getArtworks().then((gallery) => {
                    if (index < gallery.length)
                        resolve(gallery[index])
                    else
                        reject("get artwork with index out of bounds.")
                })
            }
        );
    }

    const getArtworkById = (id) => {
        return new Promise(
            (resolve, reject) => {

                artworkDbRef
                    .doc(id)
                    .get()
                    .then(
                        (doc) => {
                            if (doc.exists) {
                                resolve(doc.data())
                            }
                            else {
                                reject("No such document")
                            }
                        }
                    )
                    .catch((err) => reject(err))
            }
        );
    }

    const deleteArtworkById = (id) => {
        return new Promise(
            (resolve, reject) => {

                artworkDbRef.doc("" + id).delete()
                    .then(() => {
                        resolve(id);
                    })
                    .catch((err) => {
                        console.log("caught an error: " + err);
                        reject(err, id)
                    });
            }
        );
    }

    const editArtworkById = (id, artwork) => {
        return setArtwork(id, artwork);
    }

    const setArtwork = (id, artwork) => {
        return new Promise(
            (resolve, reject) => {

                artworkDbRef.doc("" + id).set({
                    id: id,
                    title: artwork.title,
                    imgPath: artwork.imgPath,
                    thumbnail: artwork.thumbnail,
                    description: artwork.description,
                    price: artwork.price,
                    soldCopies: artwork.soldCopies
                })
                    .then(() => resolve(artwork))
                    .catch((err) => reject(err));
            }
        );
    }

    const addArtwork = (artwork) => {
        return new Promise(
            (resolve, reject) => {
                artworkDbRef
                    .add(artwork)
                    .then(
                        (doc) => {
                            artwork.id = doc.id;
                            setArtwork(doc.id, artwork).then((artwork) => resolve(artwork));
                        }
                    )
                    .catch((err) => reject(err));
            }
        );
    }

    const onArtworkSnapshotById = (id, handlerFunction) => {
        artworkDbRef.doc("" + id).onSnapshot(
            (doc) => handlerFunction(doc.data())
        );
    }

    const onArtworkSnapshot = (handlerFunction) => {
        artworkDbRef.onSnapshot(
            (data) => {
                const items = data.docs.map((doc) => doc.data())
                handlerFunction(items)
            }
        );
    }

    return {
        getGallery,
        getArtworkByIndex,
        getArtworkById,
        deleteArtworkById,
        editArtworkById,
        setArtwork,
        addArtwork,
        onArtworkSnapshotById,
        onArtworkSnapshot
    }

}

export default GalleryRepository;