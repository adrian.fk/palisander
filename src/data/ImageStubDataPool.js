//////////////////////////////////////////      FULL RES        /////////////////////////////////////////////////
import imgAsset1 from "./assets/image/compressed/galleryImg1.png"
import imgAsset2 from "./assets/image/compressed/galleryImage2.png"
import imgAsset3 from "./assets/image/galleryImage3.jpg"
import imgAsset4 from "./assets/image/compressed/galleryImage4.png"

import imgAsset6 from "./assets/image/galleryImg6.png"
import imgAsset7 from "./assets/image/compressed/galleryImg7.jpg"
import imgAsset8 from "./assets/image/compressed/galleryImg8.png"
import imgAsset9 from "./assets/image/compressed/galleryImg9.png"

import imgAsset11 from "./assets/image/compressed/galleryImg11.jpg"
import imgAsset12 from "./assets/image/compressed/galleryImg12.jpg"
import imgAsset14 from "./assets/image/compressed/galleryImg14.jpg"
import imgAsset15 from "./assets/image/galleryImg15.jpg"
import imgAsset16 from "./assets/image/compressed/galleryImg16.jpg"
import imgAsset18 from "./assets/image/compressed/galleryImg18.png"
import imgAsset19 from "./assets/image/compressed/galleryImg19.png"
import imgAsset20 from "./assets/image/compressed/galleryImg20.jpg"
import imgAsset21 from "./assets/image/compressed/galleryImg21.jpg"
import imgAsset22 from "./assets/image/compressed/galleryImg22.png"
import imgAsset23 from "./assets/image/compressed/galleryImg23.png"
import imgAsset24 from "./assets/image/compressed/galleryImg24.jpg"
import imgAsset25 from "./assets/image/compressed/galleryImg25.jpg"
import imgAsset26 from "./assets/image/compressed/galleryImg26.jpg"
import imgAsset27 from "./assets/image/compressed/galleryImg27.png"
import imgAsset28 from "./assets/image/galleryImg28.jpg"
import imgAsset29 from "./assets/image/compressed/galleryImg29.jpg"
import imgAsset30 from "./assets/image/compressed/galleryImg30.png"
import imgAsset31 from "./assets/image/compressed/galleryImg31.png"
import imgAsset32 from "./assets/image/compressed/galleryImg32.jpg"
import imgAsset33 from "./assets/image/compressed/galleryImg33.png"
import imgAsset34 from "./assets/image/compressed/galleryImg34.png"
import imgAsset35 from "./assets/image/compressed/galleryImg35.png"
import imgAsset36 from "./assets/image/compressed/galleryImg36.jpg"
import imgAsset37 from "./assets/image/compressed/galleryImg37.png"

import imgAsset39 from "./assets/image/compressed/galleryImg39.jpg"
import imgAsset40 from "./assets/image/compressed/galleryImg40.jpg"
import imgAsset41 from "./assets/image/compressed/galleryImg41.png"
import imgAsset42 from "./assets/image/compressed/galleryImg42.jpg"
import imgAsset44 from "./assets/image/compressed/galleryImg44.jpg"
import imgAsset45 from "./assets/image/compressed/galleryImg45.jpg"
import imgAsset46 from "./assets/image/compressed/galleryImg46.jpg"
import imgAsset47 from "./assets/image/compressed/galleryImg47.png"
import imgAsset48 from "./assets/image/galleryImg48.jpg"
import imgAsset49 from "./assets/image/compressed/galleryImg49.jpg"
import imgAsset50 from "./assets/image/compressed/galleryImg50.png"
import imgAsset51 from "./assets/image/compressed/galleryImg51.png"

import imgAsset53 from "./assets/image/compressed/galleryImg53.jpg"
import imgAsset54 from "./assets/image/compressed/galleryImg54.png"
import imgAsset55 from "./assets/image/compressed/galleryImg55.png"
import imgAsset56 from "./assets/image/compressed/galleryImg56.jpg"
import imgAsset57 from "./assets/image/compressed/galleryImg57.jpg"

import imgAsset60 from "./assets/image/compressed/galleryImg60.png"
import imgAsset61 from "./assets/image/compressed/galleryImg61.png"
import imgAsset62 from "./assets/image/compressed/galleryImg62.png"
import imgAsset63 from "./assets/image/compressed/galleryImg63.jpg"
import imgAsset64 from "./assets/image/compressed/galleryImg64.jpg"
import imgAsset65 from "./assets/image/compressed/galleryImg65.png"
import imgAsset66 from "./assets/image/galleryImg66.jpg"
import imgAsset67 from "./assets/image/compressed/galleryImg67.jpg"
import imgAsset68 from "./assets/image/compressed/galleryImg68.jpg"
import imgAsset69 from "./assets/image/compressed/galleryImg69.png"
import imgAsset70 from "./assets/image/compressed/galleryImg70.png"
import imgAsset71 from "./assets/image/compressed/galleryImg71.jpg"
import imgAsset72 from "./assets/image/compressed/galleryImg72.png"
import imgAsset73 from "./assets/image/compressed/galleryImg73.jpg"
import imgAsset74 from "./assets/image/galleryImg74.jpg"

import imgAsset76 from "./assets/image/compressed/galleryImg76.png"
import imgAsset77 from "./assets/image/compressed/galleryImg77.jpg"
import imgAsset78 from "./assets/image/compressed/galleryImg78.png"
import imgAsset79 from "./assets/image/galleryImg79.jpg"
import imgAsset80 from "./assets/image/compressed/galleryImg80.png"
import imgAsset81 from "./assets/image/compressed/galleryImg81.png"
import imgAsset82 from "./assets/image/compressed/galleryImg82.png"
import imgAsset83 from "./assets/image/compressed/galleryImg83.png"
import imgAsset84 from "./assets/image/compressed/galleryImg84.png"
import imgAsset85 from "./assets/image/compressed/galleryImg85.png"
import imgAsset86 from "./assets/image/compressed/galleryImg86.png"
import imgAsset87 from "./assets/image/compressed/galleryImg87.jpg"
import imgAsset88 from "./assets/image/compressed/galleryImg88.png"

//////////////////////////////////////////      THUMBNAIL        /////////////////////////////////////////////////
import imgTnAsset1 from "./assets/image/tn/galleryImg1.png"
import imgTnAsset2 from "./assets/image/tn/galleryImage2.png"
import imgTnAsset3 from "./assets/image/tn/galleryImage3.jpg"
import imgTnAsset4 from "./assets/image/compressed/galleryImage4.png"

import imgTnAsset6 from "./assets/image/galleryImg6.png"
import imgTnAsset7 from "./assets/image/tn/galleryImg7.jpg"
import imgTnAsset8 from "./assets/image/tn/galleryImg8.png"
import imgTnAsset9 from "./assets/image/tn/galleryImg9.png"

import imgTnAsset11 from "./assets/image/tn/galleryImg11.jpg"
import imgTnAsset12 from "./assets/image/tn/galleryImg12.jpg"
import imgTnAsset14 from "./assets/image/tn/galleryImg14.jpg"
import imgTnAsset15 from "./assets/image/tn/galleryImg15.jpg"
import imgTnAsset16 from "./assets/image/tn/galleryImg16.jpg"
import imgTnAsset18 from "./assets/image/tn/galleryImg18.png"
import imgTnAsset19 from "./assets/image/tn/galleryImg19.png"
import imgTnAsset20 from "./assets/image/tn/galleryImg20.jpg"
import imgTnAsset21 from "./assets/image/tn/galleryImg21.jpg"
import imgTnAsset22 from "./assets/image/tn/galleryImg22.png"
import imgTnAsset23 from "./assets/image/tn/galleryImg23.png"
import imgTnAsset24 from "./assets/image/tn/galleryImg24.jpg"
import imgTnAsset25 from "./assets/image/tn/galleryImg25.jpg"
import imgTnAsset26 from "./assets/image/tn/galleryImg26.jpg"
import imgTnAsset27 from "./assets/image/tn/galleryImg27.png"
import imgTnAsset28 from "./assets/image/tn/galleryImg28.jpg"
import imgTnAsset29 from "./assets/image/tn/galleryImg29.jpg"
import imgTnAsset30 from "./assets/image/tn/galleryImg30.png"
import imgTnAsset31 from "./assets/image/tn/galleryImg31.png"
import imgTnAsset32 from "./assets/image/tn/galleryImg32.jpg"
import imgTnAsset33 from "./assets/image/tn/galleryImg33.png"
import imgTnAsset34 from "./assets/image/tn/galleryImg34.png"
import imgTnAsset35 from "./assets/image/tn/galleryImg35.png"
import imgTnAsset36 from "./assets/image/tn/galleryImg36.jpg"
import imgTnAsset37 from "./assets/image/tn/galleryImg37.png"

import imgTnAsset39 from "./assets/image/tn/galleryImg39.jpg"
import imgTnAsset40 from "./assets/image/tn/galleryImg40.jpg"
import imgTnAsset41 from "./assets/image/tn/galleryImg41.png"
import imgTnAsset42 from "./assets/image/tn/galleryImg42.jpg"
import imgTnAsset44 from "./assets/image/tn/galleryImg44.jpg"
import imgTnAsset45 from "./assets/image/tn/galleryImg45.jpg"
import imgTnAsset46 from "./assets/image/tn/galleryImg46.jpg"
import imgTnAsset47 from "./assets/image/tn/galleryImg47.png"
import imgTnAsset48 from "./assets/image/tn/galleryImg48.jpg"
import imgTnAsset49 from "./assets/image/tn/galleryImg49.jpg"
import imgTnAsset50 from "./assets/image/tn/galleryImg50.png"
import imgTnAsset51 from "./assets/image/tn/galleryImg51.png"

import imgTnAsset53 from "./assets/image/tn/galleryImg53.jpg"
import imgTnAsset54 from "./assets/image/tn/galleryImg54.png"
import imgTnAsset55 from "./assets/image/tn/galleryImg55.png"
import imgTnAsset56 from "./assets/image/tn/galleryImg56.jpg"
import imgTnAsset60 from "./assets/image/tn/galleryImg60.png"
import imgTnAsset61 from "./assets/image/tn/galleryImg61.png"
import imgTnAsset62 from "./assets/image/tn/galleryImg62.png"
import imgTnAsset63 from "./assets/image/tn/galleryImg63.jpg"
import imgTnAsset64 from "./assets/image/tn/galleryImg64.jpg"
import imgTnAsset65 from "./assets/image/tn/galleryImg65.png"
import imgTnAsset66 from "./assets/image/tn/galleryImg66.jpg"
import imgTnAsset67 from "./assets/image/tn/galleryImg67.jpg"
import imgTnAsset68 from "./assets/image/tn/galleryImg68.jpg"
import imgTnAsset69 from "./assets/image/tn/galleryImg69.png"
import imgTnAsset70 from "./assets/image/tn/galleryImg70.png"
import imgTnAsset71 from "./assets/image/tn/galleryImg71.jpg"
import imgTnAsset72 from "./assets/image/tn/galleryImg72.png"
import imgTnAsset73 from "./assets/image/tn/galleryImg73.jpg"
import imgTnAsset74 from "./assets/image/tn/galleryImg74.jpg"

import imgTnAsset76 from "./assets/image/tn/galleryImg76.png"
import imgTnAsset77 from "./assets/image/tn/galleryImg77.jpg"
import imgTnAsset78 from "./assets/image/tn/galleryImg78.png"
import imgTnAsset79 from "./assets/image/tn/galleryImg79.jpg"
import imgTnAsset57 from "./assets/image/tn/galleryImg57.jpg"

import imgTnAsset80 from "./assets/image/tn/galleryImg80.png"
import imgTnAsset81 from "./assets/image/tn/galleryImg81.png"
import imgTnAsset82 from "./assets/image/tn/galleryImg82.png"
import imgTnAsset83 from "./assets/image/tn/galleryImg83.png"
import imgTnAsset84 from "./assets/image/tn/galleryImg84.png"
import imgTnAsset85 from "./assets/image/tn/galleryImg85.png"
import imgTnAsset86 from "./assets/image/tn/galleryImg86.png"
import imgTnAsset87 from "./assets/image/tn/galleryImg87.jpg"
import imgTnAsset88 from "./assets/image/tn/galleryImg88.png"


const ImageStubDataPool = (function (){
    const images = [
        {title: "sugarmix", imgPath: imgAsset1, thumbnail: imgTnAsset1},
        {title: "Sup 2020?", imgPath: imgAsset29, thumbnail: imgTnAsset29},
        {title: "sugarmix", imgPath: imgAsset6, thumbnail: imgTnAsset6},
        {title: "BATMAN", imgPath: imgAsset3, thumbnail: imgTnAsset3},
        {title: "sugarmix", imgPath: imgAsset4, thumbnail: imgTnAsset4},
        {title: "sugarmix", imgPath: imgAsset15, thumbnail: imgTnAsset15},
        {title: "sugarmix", imgPath: imgAsset2, thumbnail: imgTnAsset2},
        {title: "sugarmix", imgPath: imgAsset8, thumbnail: imgTnAsset8},
        {title: "sugarmix", imgPath: imgAsset9, thumbnail: imgTnAsset9},
        {title: "sugarmix", imgPath: imgAsset11, thumbnail: imgTnAsset11},
        {title: "Solstice🌓", imgPath: imgAsset12, thumbnail: imgTnAsset12},
        {title: "sugarmix", imgPath: imgAsset14, thumbnail: imgTnAsset14},
        {title: "sugarmix", imgPath: imgAsset7, thumbnail: imgTnAsset7},
        {title: "summer & hermès", imgPath: imgAsset28, thumbnail: imgTnAsset28},
        {title: "TurmiksTV", imgPath: imgAsset25, thumbnail: imgTnAsset25},
        {title: "Soria Moria", imgPath: imgAsset16, thumbnail: imgTnAsset16},
        {title: "sugarmix", imgPath: imgAsset18, thumbnail: imgTnAsset18},
        {title: "Lets do this! 🧴🦇", imgPath: imgAsset19, thumbnail: imgTnAsset19},
        {title: "sugarmix", imgPath: imgAsset20, thumbnail: imgTnAsset20},
        {title: "Solstice🌓", imgPath: imgAsset21, thumbnail: imgTnAsset21},
        {title: "Hawaii to Oslo", imgPath: imgAsset22, thumbnail: imgTnAsset22},
        {title: "sugarmix", imgPath: imgAsset23, thumbnail: imgTnAsset23},
        {title: "Solstice🌓", imgPath: imgAsset24, thumbnail: imgTnAsset24},
        {title: "sugarmix", imgPath: imgAsset26, thumbnail: imgTnAsset26},
        {title: "sugarmix", imgPath: imgAsset27, thumbnail: imgTnAsset27},
        {title: "🌈", imgPath: imgAsset30, thumbnail: imgTnAsset30},
        {title: "sugarmix", imgPath: imgAsset31, thumbnail: imgTnAsset31},
        {title: "sugarmix", imgPath: imgAsset32, thumbnail: imgTnAsset32},
        {title: "sugarmix", imgPath: imgAsset33, thumbnail: imgTnAsset33},
        {title: "sugarmix", imgPath: imgAsset34, thumbnail: imgTnAsset34},
        {title: "Solstice🌓", imgPath: imgAsset35, thumbnail: imgTnAsset35},
        {title: "sugarmix", imgPath: imgAsset36, thumbnail: imgTnAsset36},
        {title: "sugarmix", imgPath: imgAsset37, thumbnail: imgTnAsset37},
        {title: "sugarmix", imgPath: imgAsset41, thumbnail: imgTnAsset41},
        {title: "sugarmix", imgPath: imgAsset60, thumbnail: imgTnAsset60},
        {title: "sugarmix", imgPath: imgAsset77, thumbnail: imgTnAsset77},
        {title: "sugarmix", imgPath: imgAsset39, thumbnail: imgTnAsset39},
        {title: "El director", imgPath: imgAsset46, thumbnail: imgTnAsset46},
        {title: "sugarmix", imgPath: imgAsset42, thumbnail: imgTnAsset42},
        {title: "sugarmix", imgPath: imgAsset40, thumbnail: imgTnAsset40},
        {title: "sugarmix", imgPath: imgAsset44, thumbnail: imgTnAsset44},
        {title: "sugarmix", imgPath: imgAsset45, thumbnail: imgTnAsset45},
        {title: "sugarmix", imgPath: imgAsset47, thumbnail: imgTnAsset47},
        {title: "sugarmix", imgPath: imgAsset48, thumbnail: imgTnAsset48},
        {title: "sugarmix", imgPath: imgAsset57, thumbnail: imgTnAsset57},
        {title: "Cin cin🥂🇳🇴", imgPath: imgAsset54, thumbnail: imgTnAsset54},
        {title: "#blackfriday", imgPath: imgAsset49, thumbnail: imgTnAsset49},
        {title: "sugarmix", imgPath: imgAsset50, thumbnail: imgTnAsset50},
        {title: "sugarmix", imgPath: imgAsset51, thumbnail: imgTnAsset51},
        {title: "sugarmix", imgPath: imgAsset53, thumbnail: imgTnAsset53},
        {title: "sugarmix", imgPath: imgAsset55, thumbnail: imgTnAsset55},
        {title: "🦓", imgPath: imgAsset56, thumbnail: imgTnAsset56},
        {title: "sugarmix", imgPath: imgAsset61, thumbnail: imgTnAsset61},
        {title: "Guns N’ Roses", imgPath: imgAsset62, thumbnail: imgTnAsset62},
        {title: "sugarmix", imgPath: imgAsset63, thumbnail: imgTnAsset63},
        {title: "sugarmix", imgPath: imgAsset64, thumbnail: imgTnAsset64},
        {title: "sugarmix", imgPath: imgAsset65, thumbnail: imgTnAsset65},
        {title: "sugarmix", imgPath: imgAsset66, thumbnail: imgTnAsset66},
        {title: "Attenzione .", imgPath: imgAsset67, thumbnail: imgTnAsset67},
        {title: "sugarmix", imgPath: imgAsset68, thumbnail: imgTnAsset68},
        {title: "sugarmix", imgPath: imgAsset69, thumbnail: imgTnAsset69},
        {title: "sugarmix", imgPath: imgAsset70, thumbnail: imgTnAsset70},
        {title: "sugarmix", imgPath: imgAsset71, thumbnail: imgTnAsset71},
        {title: "sugarmix", imgPath: imgAsset72, thumbnail: imgTnAsset72},
        {title: "sugarmix", imgPath: imgAsset73, thumbnail: imgTnAsset73},
        {title: "sugarmix", imgPath: imgAsset74, thumbnail: imgTnAsset74},
        {title: "Close your eyes(and feel)", imgPath: imgAsset76, thumbnail: imgTnAsset76},
        {title: "summer & hermès", imgPath: imgAsset78, thumbnail: imgTnAsset78},
        {title: "sugarmix", imgPath: imgAsset79, thumbnail: imgTnAsset79},
        {title: "sugarmix", imgPath: imgAsset80, thumbnail: imgTnAsset80},
        {title: "sugarmix", imgPath: imgAsset81, thumbnail: imgTnAsset81},
        {title: "sugarmix", imgPath: imgAsset82, thumbnail: imgTnAsset82},
        {title: "sugarmix", imgPath: imgAsset83, thumbnail: imgTnAsset83},
        {title: "sugarmix", imgPath: imgAsset84, thumbnail: imgTnAsset84},
        {title: "sugarmix", imgPath: imgAsset85, thumbnail: imgTnAsset85},
        {title: "sugarmix", imgPath: imgAsset86, thumbnail: imgTnAsset86},
        {title: "sugarmix", imgPath: imgAsset87, thumbnail: imgTnAsset87},
        {title: "sugarmix", imgPath: imgAsset88, thumbnail: imgTnAsset88},

    ]









    const getByTitle = (title) => {
        images.forEach(
            (img) => {
                if (img.title === title) return img;
            }
        )
    }

    const getData = () => images;

    const get = (index) => {
        if (index) {
            if (index < images.length) return images[index];
        }
        else {
            return getData();
        }
    }


    return { getData, getByTitle, get }
} ())

export default ImageStubDataPool;