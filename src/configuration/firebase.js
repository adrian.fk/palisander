import firebase from "firebase";
import "firebase/firestore";
import FirebaseConfig from "./FirebaseConfig";


const firebaseApp = firebase.initializeApp(FirebaseConfig);
const db = firebaseApp.firestore();
const auth = firebase.auth();
const provider = new firebase.auth.GoogleAuthProvider();


export { auth, provider };

export default db;