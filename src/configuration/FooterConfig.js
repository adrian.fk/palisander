const FooterConfig = (function () {

    const copyrightStatement = "© Copyright 2021. All rights reserved. Developed & Designed by Adrian Falch Karlsen.";



    const getConfig = () => {
        return { copyrightStatement };
    }

    return { getConfig, copyrightStatement };

} ());

export default FooterConfig;