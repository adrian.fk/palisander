import AdminUser from "../model/Admin/AdminUser";


const AdminUserAdapter = () => {

    const mapToAdminUser = (id, email, displayName) => {
        const user = new AdminUser();

        user.id = id;
        user.email = email;
        user.displayName = displayName;

        return user;
    }

    return { mapToAdminUser }
}

export default AdminUserAdapter;