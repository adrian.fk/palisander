

export default
class AdminUser {
    constructor() {
        this._email = "";
        this._id = ""
        this._displayName = "";
    }

    get id() {
        return this._id;
    }

    set id(id) {
        this._id = id;
    }

    get email() {
        return this._email;
    }

    set email(email) {
        this._email = email;
    }

    get displayName() {
        return this._displayName;
    }

    set displayName(displayName) {
        this._displayName = displayName;
    }

    get isLoggedIn() {
        return this._email !== "" && this._id !== "";
    }
}