

export default class User {

    constructor() {
        this.curProdViewing = {
            title: "",
            imgPath: "",
        };
        this.curViewingProd = false;
    }

    static getInstance() {
        if (!User.INSTANCE) {
            User.INSTANCE = new User();
        }
        return User.INSTANCE;
    }

    setCurrentProdView(title, imgPath) {
        this.curProdViewing = {
            title: title,
            imgPath: imgPath,
        };
    }

    setCurrentlyViewingProduct(boolIsViewing = false) {
        this.curViewingProd = boolIsViewing;
    }

    isViewingProd() {
        return this.curViewingProd;
    }

    getCurrentProductViewingObj() {
        return this.curProdViewing;
    }

    getCurrentProductViewingTitle() {
        return this.curProdViewing.title;
    }

    getCurrentProductViewingImgPath() {
        return this.curProdViewing.imgPath;
    }


}