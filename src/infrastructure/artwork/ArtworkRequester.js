import ArtworkRequest, {
    DELETE_ARTWORK,
    REQUEST_ARTWORK,
    REQUEST_ARTWORK_GALLERY,
    SET_ARTWORK,
} from "./ArtworkRequest";
import ArtworkGateway from "./ArtworkGateway";


const ArtworkRequester = () => {

    function handleArtworkGalleryRequest(resolve) {
        const gallery = ArtworkGateway().getGallery();
        resolve(gallery);
    }

    function handleArtworkRequestInstruction(instr, resolve, reject) {

        switch (instr.op) {

            case "DELETE":
                ArtworkGateway()
                    .deleteArtworkById(instr.id)
                    .then((id) => resolve(id))
                    .catch(err => reject(err));
                break;

            case "SET":
                ArtworkGateway()
                    .updateArtworkById(instr.id, instr.artwork)
                    .then((id) => resolve(id))
                    .catch(err => reject(err))
                break;

            case "GET":
                if (instr.index) {
                    ArtworkGateway()
                        .getArtworkByIndex(instr.index)
                        .then((artwork) => resolve(artwork));
                    break;
                }
                else if (instr.name) {

                    break;
                }

            default:
                reject();
                break;
        }
    }

    const processRequest = (req) => {
        return new Promise(
            (resolve, reject) => {
                switch (req.type) {

                    case REQUEST_ARTWORK_GALLERY:
                        handleArtworkGalleryRequest(resolve);
                        break;

                    case DELETE_ARTWORK:
                        req.instructions.forEach(instr => handleArtworkRequestInstruction(instr, resolve, reject))
                        break;

                    case REQUEST_ARTWORK:
                        req.instructions.forEach((instr) => handleArtworkRequestInstruction(instr, resolve, reject))
                        break;

                    case SET_ARTWORK:
                        req.instructions.forEach( (instr) => handleArtworkRequestInstruction(instr, resolve, reject))
                        break;

                    default:
                        reject();
                }
            }
        );
    }

    const request = (req = new ArtworkRequest()) => {
        return new Promise(
            (resolve, reject) => {
                if (req && req.type && req.instructions) {
                    processRequest(req)
                        .then((gallery) => resolve(gallery));
                }
                else {
                    reject("Invalid request");
                }
            }
        );
    }

    const fetchArtworkGallery = () => {
        return new Promise(
            (resolve, reject) => {
                const requestObj = new ArtworkRequest(REQUEST_ARTWORK_GALLERY);

                request(requestObj)
                    .then((gallery) => {
                        resolve(gallery);
                    })
                    .catch((err) => reject(err));
            }
        );
    }

    const fetchArtworkByIndex = (index) => {
        return new Promise(
            (resolve) => {
                const requestObj = new ArtworkRequest(REQUEST_ARTWORK);
                requestObj.addInstruction({op: "GET", index});

                request(requestObj)
                    .then((artwork) => resolve(artwork));
            }
        );
    }

    const fetchArtworkById = (id) => {
        return ArtworkGateway().getArtworkById(id);
    }

    const setArtworkById = (id, artwork) => {
        return new Promise(
            (resolve, reject) => {
                const req = new ArtworkRequest(SET_ARTWORK);
                req.addInstruction({op: "SET", index: id - 1, id: id, artwork: artwork})

                request(req)
                    .then((id) => resolve(id))
                    .catch((err) => reject(err));
            }
        );
    }

    const delArtworkById = (id) => {
        return new Promise(
            (resolve, reject) => {
                const req = new ArtworkRequest(DELETE_ARTWORK);
                req.addInstruction({op: "DELETE", id: id, index: id - 1});

                request(req)
                    .then(id => resolve(id))
                    .catch(err => reject(err));
            }
        );
    }

    const onSnapshot = (handlerFunction) => {
        /*
        const req = new ArtworkRequest(SNAPHOT_ARTWORK);
        req.addInstruction({
            op: "SNAPSHOT",
            id: id,
            index: id - 1,
            handler: handlerFunction
        });

        request(req);
        */

        ArtworkGateway()
            .onSnapshot(handlerFunction);
    }

    const addArtwork = (artwork) => {
        return ArtworkGateway().addArtwork(artwork)
    }


    return {
        request,
        fetchArtworkGallery,
        fetchArtworkByIndex,
        fetchArtworkById,
        setArtworkById,
        delArtworkById,
        onSnapshot,
        addArtwork
    };
}

export default ArtworkRequester;