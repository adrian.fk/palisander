import GalleryRepository from "../../data/GalleryRepository";


const ArtworkGateway = () => {

    const getGallery = () => {
        return GalleryRepository().getGallery();
    }

    const getArtworkByIndex = (index) => {
        return GalleryRepository().getArtworkByIndex(index);
    }

    const getArtworkById = (id) => {
        return GalleryRepository().getArtworkById(id);
    }

    const deleteArtworkById = (id) => {
        return new Promise(
            (resolve, reject) => {

                GalleryRepository()
                    .deleteArtworkById(id)
                    .then((idDeleted) => resolve(idDeleted))
                    .catch((err) => reject(err));
            }
        );
    }

    const onSnapshot = (handlerFunction) => {
        GalleryRepository().onArtworkSnapshot(handlerFunction);
    }

    const updateArtworkById = (id, artwork) => {
        return GalleryRepository().setArtwork(id, artwork);
    }

    const addArtwork = (artwork) => {
        return GalleryRepository().addArtwork(artwork);
    }

    return { getGallery, getArtworkByIndex, getArtworkById, deleteArtworkById, updateArtworkById, onSnapshot, addArtwork };
}

export default ArtworkGateway;