export const REQUEST_ARTWORK_GALLERY = "artworkGallery";
export const REQUEST_ARTWORK = "artwork";
export const SET_ARTWORK = "setArtwork";
export const DELETE_ARTWORK = "delArtwork";
export const SNAPHOT_ARTWORK = "snapshotArtwork";


export default class ArtworkRequest {
    constructor(type = "", instructions = []) {
        this.type = type;
        this.instructions = instructions;
    }

    setType(type) {
        this.type = type;
    }

    addInstruction(instr) {
        this.instructions.push(instr)
    }

    popInstruction() {
        return this.instructions.pop();
    }

    clear() {
        this.type = "";
        this.instructions = [];
    }
};
