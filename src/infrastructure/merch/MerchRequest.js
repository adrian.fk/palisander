export const REQUEST_MERCH_GALLERY = "merchGallery";
export const REQUEST_MERCH = "merch";


export default class MerchRequest {
    constructor(type = "", instructions = []) {
        this.type = type;
        this.instructions = instructions;
    }

    setType(type) {
        this.type = type;
    }

    addInstruction(instr) {
        this.instructions.push(instr)
    }

    popInstruction() {
        return this.instructions.pop();
    }

    clear() {
        this.type = "";
        this.instructions = [];
    }
};
