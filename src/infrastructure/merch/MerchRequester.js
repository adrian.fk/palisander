import MerchRequest, {REQUEST_MERCH, REQUEST_MERCH_GALLERY} from "./MerchRequest";
import MerchGateway from "./MerchGateway";


const MerchRequester = () => {

    function handleMerchGalleryRequest(resolve) {
        const gallery = MerchGateway().getMerch();
        resolve(gallery);
    }

    function handleMerchRequestInstruction(instr, resolve, reject) {
        switch (instr.op) {

            case "GET":
                if (instr.index) {
                    const merch = MerchGateway().getMerchByIndex(instr.index);
                    resolve(merch);
                    break;
                }
                else if (instr.id) {
                    const merch = MerchGateway().getMerchById(instr.id);
                    resolve(merch);
                }
                else if (instr.name) {

                }
                break;

            default:
                reject();
                break;
        }
    }

    const processRequest = (req) => {
        return new Promise(
            (resolve, reject) => {
                switch (req.type) {

                    case REQUEST_MERCH_GALLERY:
                        handleMerchGalleryRequest(resolve);
                        break;


                    case REQUEST_MERCH:
                        req.instructions.forEach((instr) => handleMerchRequestInstruction(instr, resolve, reject))
                        break;

                    default:
                        reject();
                }
                resolve();
            }
        );
    }

    const request = (req = new MerchRequest()) => {
        return new Promise(
            (resolve, reject) => {
                if (req && req.type && req.instructions) {
                    processRequest(req)
                        .then((gallery) => resolve(gallery));
                }
                else {
                    reject("Invalid request");
                }
            }
        );
    }

    const fetchMerchGallery = () => {
        return new Promise(
            (resolve, reject) => {
                const requestObj = new MerchRequest(REQUEST_MERCH_GALLERY);

                request(requestObj)
                    .then((gallery) => {
                        resolve(gallery);
                    })
                    .catch((err) => reject(err));
            }
        );
    }

    const fetchMerchByIndex = (index) => {
        return new Promise(
            (resolve) => {
                const requestObj = new MerchRequest(REQUEST_MERCH);
                requestObj.addInstruction({op: "GET", index});

                request(requestObj)
                    .then((artwork) => resolve(artwork));
            }
        );
    }

    const fetchMerchById = (id) => {
        return new Promise(
            (resolve) => {
                const requestObj = new MerchRequest(REQUEST_MERCH);
                requestObj.addInstruction({op: "GET", id});

                request(requestObj)
                    .then((artwork) => resolve(artwork));
            }
        );
    }

    const onSnapshot = (handler) => {
        return MerchGateway().onSnapshot(handler);
    }

    const delById = (id) => {
        return MerchGateway().delById(id);
    }

    const setById = (id, merch) => {

    }

    const addMerch = (merch) => {
        return MerchGateway().addMerch(merch);
    }


    return { addMerch, delById, request, fetchMerchGallery, fetchMerchByIndex, fetchMerchById, onSnapshot };
}

export default MerchRequester;