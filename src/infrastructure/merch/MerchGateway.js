import MerchRepository from "../../data/MerchRepository";


const MerchGateway = () => {

    const getMerch = () => {
        return MerchRepository().getMerch();
    }

    const getMerchByIndex = (index) => {
        return MerchRepository().getArtworkByIndex(index);
    }

    const getMerchById = (id) => {
        return MerchRepository().getById(id);
    }

    const onSnapshot = (handler) => {
        MerchRepository().onMerchSnapshot(handler);
    }

    const delById = (id) => MerchRepository().deleteById(id);

    const addMerch = (merch) => {
        return MerchRepository().addMerch(merch);
    }

    return { addMerch, getMerch, getMerchByIndex, getMerchById, onSnapshot, delById };
}

export default MerchGateway;