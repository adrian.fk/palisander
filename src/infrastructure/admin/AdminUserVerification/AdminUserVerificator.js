import AdminUserManager from "../AdminUserManager";


const AdminUserVerificator = () => {

    const attemptUserVerification = (user, users) => {

        let userFound = false;

        users.forEach(
            (usr) => {

                if (usr.id === user.id) userFound = true;
            }
        )

        return userFound;
    }

    const verifyUser = (user) => {
        return new Promise(
            (resolve, reject) => {

                if (user) {
                    AdminUserManager().getUsers()
                        .then(
                            (users) => {
                                if(attemptUserVerification(user, users)) resolve(user);
                                else {
                                    reject({type: "NOT_FOUND", error: "could not find user"})
                                }
                            }
                        )
                        .catch(
                            (err) => reject({type: "NO_CONNECTION", error: err})
                        )

                }
                else reject({type: "INTERNAL", error: "no user passed to verify.."})
            }
        );
    }

    return { verifyUser };

}

export default AdminUserVerificator;