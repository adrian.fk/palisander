import AdminUserRepository from "../../data/AdminUserRepository";
import AdminUserProxy from "./AdminUserProxy";

function AdminUserGateway() {

    const adminUserRepository = AdminUserRepository();



    const getAdminUsers = () => adminUserRepository.getAdminUsers();

    const getUserCurrentSession = () => AdminUserProxy.getUser();




    return { getAdminUsers, getUserCurrentSession }

}

export default AdminUserGateway;
