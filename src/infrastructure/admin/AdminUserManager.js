import AdminUserGateway from "./AdminUserGateway";
import {auth, provider} from "../../configuration/firebase";


const AdminUserManager = () => {

    const gateway = AdminUserGateway();

    const getUsers = () => gateway.getAdminUsers();

    const loginWithPopup = () => {
        return new Promise(
            (resolve, reject) => {
                auth.signInWithPopup(provider)
                    .then(
                        res => {
                            resolve(res);
                        }
                    )
                    .catch(
                        err => {
                            alert(err);
                            reject(err);
                        }
                    )
            }
        );
    }


    return { getUsers, loginWithPopup };

}

export default AdminUserManager;
