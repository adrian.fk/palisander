import AdminUserStorage from "../../data/AdminUserStorage";


function AdminUserProxy() {
    const adminUserStorage = AdminUserStorage.getInstance();

    const getUser = () => adminUserStorage.getUser();

    const setUser = (user) => {
        adminUserStorage.setUser(user);
        return this;
    }

    return { getUser, setUser }

}

export default AdminUserProxy;