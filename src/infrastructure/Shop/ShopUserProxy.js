import User from "../../domain/model/Shop/User";

//Simulates a user session handler

export default
class ShopUserProxy {

    static portUserViewToShop(resource = {title: "", imgPath: ""}) {
        return new Promise(
            (resolve) => {
                const user = User.getInstance();
                user.setCurrentProdView(resource.title, resource.imgPath);
                user.setCurrentlyViewingProduct(true);
                if (resolve) resolve();
            }
        );
    }

    static hasPortedUserView() {
        return new Promise(
            (accept, reject) => {
                const user = User.getInstance();
                if (user.isViewingProd()) {
                    if (accept) accept();
                }
                else {
                    if (reject) reject();
                }
            }
        );
    }

    static getPortedUserView() {
        return new Promise(
            (resolve, reject) => {
                const user = User.getInstance();
                const currentProductViewingObj = user.getCurrentProductViewingObj();
                if (user.isViewingProd() && currentProductViewingObj) {
                    if (resolve) resolve(currentProductViewingObj);
                }
                else {
                    if (reject) reject("Could not find view to display");
                }
            }
        );
    }

}