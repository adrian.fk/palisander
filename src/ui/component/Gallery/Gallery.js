import React from 'react';
import Header from "../../Header/Header";
import Footer from "../../Footer/Footer";
import GalleryMatrixView from "./widgets/GalleryMatrixView";


export default
class Gallery extends React.Component{

    constructor(props) {
        super(props);
        this.className = "gallery"
        this.state = {
            content: < GalleryMatrixView />
        }
    }

    setContentComponent(component) {
        this.setState({
            content: component
        });
    }

    render() {
        return (
            <div className={this.className}>
                < Header menuEntry={this.className} />
                {this.state.content}
                < Footer />
            </div>
        );
    }
}