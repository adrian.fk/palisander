import React from 'react';
import GalleryController from "../../../controller/Gallery/GalleryController";
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';

import '../Gallery.css'

export default
class GalleryMatrixCellView extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            id: props.id,
            title: props.title,
            imgTnPath: props.imgTnPath
        }
        this.controller = GalleryController.getInstance();
    }

    handleCardClick() {
        const cardElem = document.getElementById("card-" + this.state.id);
        if (cardElem) {
            cardElem.classList.add("card--enlarge");

            setTimeout(
                () => {
                    cardElem.classList.remove("card--enlarge");
                    this.controller.handleCardClick(this.state.id);
                },
                200
            )
        }
        else {
            this.controller.handleCardClick(this.state.id);
        }
    }

    render() {
        return (
            <section
                id={"card-" + this.state.id}
                className={"gallery-matrix-cell column is-4"}
                onClick={() => this.handleCardClick()}
            >

                <LazyLoadImage
                    effect={"blur"}
                    wrapperClassName={"img"}
                    alt={""}
                    src={this.state.imgTnPath}
                    placeholderSrc={this.state.imgTnPath}
                    width={"100%"}
                    height={"100%"}
                />

            </section>
        );
    }
}