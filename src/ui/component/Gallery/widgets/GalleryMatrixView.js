import React from 'react';
import GalleryController from "../../../controller/Gallery/GalleryController";
import GalleryMatrixCellView from "./GalleryMatrixCellView";
import GalleryFullscreenView from "./GalleryFullscreenView";

export default
class GalleryMatrixView extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.controller = GalleryController.getInstance();
        this.controller.bind(this);

        this.controller.promiseGalleryEntries()
            .then((elements) => this.setState({
                renderElements: elements,
            }));



        this.state = {
            renderElements: null,
            fullscreen: false,
            fullscreenComponent: null
        }
    }

    displayFullscreen() {
        this.setState({
            fullscreen: true
        });
        window.scrollTo(0, 0);
    }

    exitFullscreen() {
        this.setState({
            fullscreen: false
        });
    }

    setFullscreenComponent(id, title, imgPath, imgTnPath, componentStatement) {
        if (!componentStatement) {
            this.setState({
                fullscreenComponent: <GalleryFullscreenView title={title} imgPath={imgPath} imgTnPath={imgTnPath} />
            });
        }
        else {
            this.setState({
                fullscreenComponent: componentStatement
            });
        }
    }

    setAndDisplayFullscreenComponent(id, title, imgPath, imgTnPath, componentStatement) {
        this.setFullscreenComponent(id, title, imgPath, imgTnPath, componentStatement);
        this.displayFullscreen();
    }

    render() {
        return (
            <div className={"gallery_matrix"}>
                <div className={"container"}>
                    <section className={"columns has-3 is-multiline"}>
                        {this.state.renderElements ? this.state.renderElements.map(
                            (element, index) =>
                                < GalleryMatrixCellView key={index} id={index} title={element.title} imgTnPath={element.thumbnail} />
                        ) : ""}
                    </section>
                </div>
                { this.state.fullscreen ? this.state.fullscreenComponent : "" }
            </div>
        );
    }
}
