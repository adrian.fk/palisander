import React, {useState} from 'react';
import GalleryController from "../../../controller/Gallery/GalleryController";
import ShopUserProxy from "../../../../infrastructure/Shop/ShopUserProxy";
import User from "../../../../domain/model/Shop/User";
import  { Redirect } from 'react-router-dom'
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';


function GalleryFullscreenView(props) {

    const [state, setState] = useState({
        title: props.title,
        imgPath: props.imgPath,
        imgTnImg: props.imgTnPath,
        redirectToShop: false,
    });


    const controller = GalleryController.getInstance();


    const onShopBtnClick = (e) => {
        e.stopPropagation();

        const user = new User();
        user.setCurrentProdView(state.title, state.imgPath);
        user.setCurrentlyViewingProduct(true);


        ShopUserProxy
            .portUserViewToShop({title: state.title, imgPath: state.imgPath})
            .then(
                () => setState({...state, redirectToShop: true})
            )
    }

    if (state.redirectToShop) {
        return (
            <Redirect push to='./shop' />
        )
    }
    else
    return (
        <div className={"gallery_matrix_fullscreen"} onClick={() => controller.handleExitFullscreen()}>
            <div className={"container--fullscreen"}>
                <section className={"columns"}>


                    <section className={"gallery_matrix_fullscreen_cell column"}>
                        <div className="card m-3" >
                            <div className="card-image">
                                <figure className={"image fullscreen-img"} >
                                    <LazyLoadImage
                                        effect={"blur"}
                                        src={state.imgPath}
                                        placeholderSrc={state.imgPath}
                                        alt={""}
                                    />
                                </figure>
                            </div>
                            {/*
                            <div className="card-content p-3">
                                <div className="media">
                                    <div className="media-content">
                                        <p className={"card-title title is-4"}>{state.title}</p>
                                    </div>
                                </div>
                            </div>
                            */}
                        </div>
                    </section>
                </section>

                <section className={"columns"}>

                    <section className={"gallery_matrix_fullscreen_cell column"}>
                        <button
                            className={"button is-black is-large"}
                            onClick={(e) => onShopBtnClick(e)}
                        >
                            Shop
                        </button>
                    </section>
                </section>
            </div>
        </div>
    );
}

export default GalleryFullscreenView;
