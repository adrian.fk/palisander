import React from 'react';
import Header from "../../Header/Header";
import Footer from "../../Footer/Footer";
import HomeContainer from "./HomeContainerView";

import "./Home.css";

function Home() {
 return (
   <div className={"home"}>
     < Header menuEntry={"home"} />
       < HomeContainer />
       < Footer />
   </div>
 );
}

export default Home;