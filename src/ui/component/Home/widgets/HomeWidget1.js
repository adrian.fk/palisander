import React, {useState} from 'react';

import { LazyLoadComponent } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';

import Video from "../assets/closeYourEyesAndSee.mp4";
import '../HomeCointainer.css'
import './HomeWidget.css'


function HomeWidget1() {

    const [isVideoLoaded, videoLoaded] = useState(false);



    const onVideoLoaded = () => {
        videoLoaded(true);
    }


    return (
       <div
           className={"home_widget home_widget1"}
       >
           <div
               className={"home_widget__container" + (isVideoLoaded ? " show" : "")}
               style={{opacity: isVideoLoaded ? 1 : 0}}
           >
               <video className={"card container__video"} src={Video} loop={true} autoPlay={true} muted={true} onLoadedData={e => onVideoLoaded()} />
               {/*<div className={"container__title container__title--mobile is-hidden-tablet"}>*/}
               {/*    <h2>Close your eyes(and feel)</h2>*/}
               {/*</div>*/}
               {/*<div className={"container__title is-hidden-mobile"}>*/}
               {/*    <h2>Close your eyes(and feel)</h2>*/}
               {/*</div>*/}
           </div>
       </div>
 );
}

export default HomeWidget1;