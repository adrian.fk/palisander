import React, {useState} from 'react';
import ImageStubDataPool from "../../../../data/ImageStubDataPool";

import 'react-lazy-load-image-component/src/effects/blur.css';

import '../HomeCointainer.css'
import './HomeWidget.css'


function HomeWidget2(props) {

    const [isImageLoaded, imageLoaded] = useState(false);

    let imgObj;
    if (!props.targetIndex) {
        imgObj = ImageStubDataPool.get(40);
    }
    else {
        imgObj = ImageStubDataPool.get(props.targetIndex);
    }

    const onImageLoaded = () => {
        imageLoaded(true);
    }

    return (
        <div
            className={"home_widget home_widget2" + (isImageLoaded ? " show" : "")}
            style={{opacity: isImageLoaded ? 1 : 0}}
        >
            <img
                src={imgObj.imgPath}
                alt={""}
                onLoad={(e) => onImageLoaded()}
            />

        </div>
    );
}

export default HomeWidget2;