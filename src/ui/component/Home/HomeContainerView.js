import React from 'react';

import './HomeCointainer.css'

import HomeController from "../../controller/Home/HomeController";

import ArrowBackIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIcon from '@material-ui/icons/ArrowForwardIos';

import HomeWidget1 from "./widgets/HomeWidget1";
import HomeWidget2 from "./widgets/HomeWidget2";


export default
class HomeContainerView extends React.Component {

    constructor(props) {
        super(props);

        const widgets = {
            //widget1: <HomeWidget1 />,
            widget2: <HomeWidget2 targetIndex={53}/>,
            widget3: <HomeWidget2 targetIndex={6}/>,
            widget4: <HomeWidget2 targetIndex={33}/>,
            widget5: <HomeWidget2 targetIndex={28}/>,
        }

        this.state = {
            activeWidget: widgets["widget2"],
            widgets: widgets
        }

        this.controller = new HomeController(5).bind(this);
    }

    setWidget(id) {
        const widgets = this.state.widgets;
        this.setState({
            activeWidget: widgets[id]
        });
    }

    render() {

        return (
            <div className={"home__container"} >
                <div className={"container__arrow container__arrow_back"} onClick={() => this.controller.nav("back")} >
                    <ArrowBackIcon />
                </div>
                <div className={"container__arrow container__arrow_forward"} onClick={() => this.controller.nav(this.controller.nav().next)} >
                    <ArrowForwardIcon />
                </div>
                <div className={"widget is-hidden-mobile"} onClick={() => this.controller.handleBtnClick()}>
                    {this.state.activeWidget}
                </div>
                <div className={"widget--mobile is-hidden-tablet"} onClick={() => this.controller.handleBtnClick()}>
                    {this.state.activeWidget}
                </div>
            </div>
        );
    }
}
