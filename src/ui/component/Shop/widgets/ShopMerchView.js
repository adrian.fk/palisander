import React, {Component} from 'react';
import ShopGalleryCellView from "./ShopGalleryCellView";
import ShopController from "../../../controller/Shop/ShopController";
import ShopFullscreenView from "./ShopFullscreenView";

class ShopMerchView extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            renderElements: null
        }


        this.controller = ShopController.getInstance();
        this.controller.bind(this);

        this.controller.promiseMerchEntries()
            .then((elements) => this.setState({
                renderElements: elements,
            }));

        this.state = {
            renderElements: null,
            fullscreen: false,
            fullscreenComponent: "",
            initialCapture: true,
        };

    }


    displayFullscreen() {
        this.setState({
            fullscreen: true
        });
        window.scrollTo(0, 0);
        //this.controller.scrollSmoothY(window.scrollY, 0);
    }

    exitFullscreen() {
        this.setState({
            fullscreen: false
        });
    }

    setFullscreenComponent(id, title, imgPath, imgTnPath, componentStatement) {
        if (!componentStatement) {
            this.setState({
                fullscreenComponent: <ShopFullscreenView type={"merch"} title={title} imgPath={imgPath} imgTnPath={imgTnPath} />
            });
        }
        else {
            this.setState({
                fullscreenComponent: componentStatement
            });
        }
    }

    setAndDisplayFullscreenComponent(id, title, imgPath, imgTnPath, componentStatement) {
        this.setFullscreenComponent(id, title, imgPath, componentStatement);
        this.displayFullscreen();
    }


    render() {
        return (
            <div className={"shop__merch"}>
                <div className={"container"}>
                    <section className={"columns has-4 is-multiline"}>
                        {this.state.renderElements ? this.state.renderElements.map(
                            (element, index) =>
                                < ShopGalleryCellView
                                    key={index}
                                    type={"merch"}
                                    index={index}
                                    id={element.id}
                                    title={element.title}
                                    imgTnPath={element.thumbnail}
                                />
                        ) : ""}
                    </section>
                </div>
                { this.state.fullscreen ? this.state.fullscreenComponent : "" }
            </div>
        );
    }
}

export default ShopMerchView;