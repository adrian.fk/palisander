import React from 'react';
import PersonIcon from "@material-ui/icons/Person";
import EmailIcon from "@material-ui/icons/Email";
import HomeIcon from "@material-ui/icons/Home";
import AspectRatioIcon from "@material-ui/icons/AspectRatio";
import PhotoAlbumIcon from "@material-ui/icons/PhotoAlbum";
import FullscreenFormHiddenFieldsView from "./FullscreenFormHiddenFieldsView";
import ShopController from "../../../../controller/Shop/ShopController";
import OrderBtnDeactive from "../OrderBtn/OrderBtnDeactive";
import OrderBtnNormal from "../OrderBtn/OrderBtnNormal";
import OrderBtnConfirmed from "../OrderBtn/OrderBtnConfirmed";
import ArtworkShopController from "../../../../controller/Shop/ArtworkShopController";

const STATE_ID_ORDER_BTN_DEACTIVATED = "deactivated";
const STATE_ID_ORDER_BTN_NORMAL = "normal";
const STATE_ID_ORDER_BTN_CONFIRMED = "confirmed";

export default
class ArtworkFullscreenForm extends React.Component {


    constructor(props, context) {
        super(props, context);


        this.shopController = ShopController.getInstance();
        this.artworkShopController = ArtworkShopController.getInstance();

        this.artworkShopController.bindView("ArtworkFullscreenForm", this);


        //TODO integrate in db entry
        this.printSizeDefault = "Print size..";
        this.printSizeLarge = "Large (60cm x 90cm)";
        this.printSizeMedium = "Medium (50cm x 70cm)";
        this.printSizeSmall = "Small (40cm x 60cm)";

        this.printMaterialDefault = "Print material.."
        this.printMaterialCanvas = "Canvas"
        this.printMaterialPlexiglas = "Plexiglas"



        this.state = {
            title: props.title,
            imgPath: props.imgPath,
            imgTnPath: props.imgTnPath,

            formBtnState: STATE_ID_ORDER_BTN_DEACTIVATED,
            formBtnRender: <OrderBtnDeactive />,

            userName: "",
            userEmail: "",
            userAddress: "",
            printSize: this.printSizeDefault,
            printMaterial: this.printMaterialDefault,
        };
    }


    setFormRenderState(stateId) {
        let renderComponent;
        switch (stateId) {
            case STATE_ID_ORDER_BTN_DEACTIVATED:
                renderComponent = <OrderBtnDeactive />
                break;

            case STATE_ID_ORDER_BTN_NORMAL:
                renderComponent = <OrderBtnNormal />
                break;

            case STATE_ID_ORDER_BTN_CONFIRMED:
                renderComponent = <OrderBtnConfirmed />
                break;

            default:
                break;
        }

        if (renderComponent) {
            this.setState({
                formBtnState: stateId,
                formBtnRender: renderComponent,
            });
        }
    }

    checkIfOrderFormReady() {

        setTimeout(
            () => {
                if (this.state.userName !== ""
                    && this.state.userEmail !== ""
                    && this.state.userAddress !== ""
                    && this.state.printSize !== this.printSizeDefault
                    && this.state.printMaterial !== this.printMaterialDefault) {

                    this.setFormRenderState(STATE_ID_ORDER_BTN_NORMAL);
                }
                else {
                    this.setFormRenderState(STATE_ID_ORDER_BTN_DEACTIVATED);
                }
            },
            100
        )
    }

    onNameChanged(e) {
        this.setState({
            userName: e.target.value
        });
        this.checkIfOrderFormReady();
    }

    onEmailChanged(e) {
        this.setState({
            userEmail: e.target.value
        });
        this.checkIfOrderFormReady();
    }

    onAddressChanged(e) {
        this.setState({
            userAddress: e.target.value
        });
        this.checkIfOrderFormReady();
    }

    printSizeChanged(event) {
        this.setState({
            printSize: event.target.value
        });
        this.checkIfOrderFormReady();
    }

    printMaterialChanged(event) {
        this.setState({
            printMaterial: event.target.value
        });
        this.checkIfOrderFormReady();
    }

    displayOrderSent() {
        this.setFormRenderState(ArtworkFullscreenForm.STATE_ID_ORDER_BTN_CONFIRMED);
    }

    render() {
        return (
            <form
                className={"shop__order-form"}
                onSubmit={(e) => this.shopController.sendOrder(e, () => this.displayOrderSent())}
            >
                <input type="hidden" name="contact_number" />
                <div className={"columns"}>
                    <div className={"column"}>
                        <div className="field">
                            <div className="control has-icons-left">
                                <input className={"input is-large"}
                                       type="text"
                                       placeholder={"Name"}
                                       name="user_name"
                                       onChange={(e) => this.onNameChanged(e)}
                                />
                                <span className="icon is-small is-left">
                                                    < PersonIcon />
                                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={"columns"}>
                    <div className={"column"}>
                        <div className="field">
                            <div className="control has-icons-left">
                                <input className={"input is-large"}
                                       type="email"
                                       placeholder={"Email"}
                                       name="user_email"
                                       onChange={(e) => this.onEmailChanged(e)}
                                />
                                <span className="icon is-small is-left">
                                                    < EmailIcon />
                                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={"columns"}>
                    <div className={"column"}>
                        <div className="field">
                            <div className="control has-icons-left">
                                <input className={"input is-large"}
                                       type="text"
                                       placeholder={"Address"}
                                       name="user_address"
                                       onChange={(e) => this.onAddressChanged(e)}
                                />
                                <span className="icon is-small is-left">
                                                    < HomeIcon />
                                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="columns">
                    <div className="column">
                        <div className="field">
                            <div className="control has-icons-left">
                                {/*Select print size*/}
                                <div className="select is-large is-fullwidth is-black" >
                                    <select onChange={(e) => this.printSizeChanged(e)}>
                                        <option>{this.printSizeDefault}</option>
                                        <option>{this.printSizeLarge}</option>
                                        <option>{this.printSizeMedium}</option>
                                        <option>{this.printSizeSmall}</option>
                                    </select>
                                </div>
                                <div className="icon is-small is-left">
                                    <AspectRatioIcon />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="columns">
                    <div className="column">
                        <div className="field">
                            <div className="control has-icons-left">
                                {/*Select print material*/}
                                <div className="select is-large is-fullwidth is-black" >
                                    <select onChange={(e) => this.printMaterialChanged(e)}>
                                        <option>{this.printMaterialDefault}</option>
                                        <option>{this.printMaterialCanvas}</option>
                                        <option>{this.printMaterialPlexiglas}</option>
                                    </select>
                                </div>
                                <div className="icon is-small is-left">
                                    <PhotoAlbumIcon />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {/*hidden fields*/}
                < FullscreenFormHiddenFieldsView
                    printSize={this.state.printSize}
                    printMaterial={this.state.printMaterial}
                    title={this.state.title}
                    imgPath={this.state.imgPath}
                />

                {this.state.formBtnRender}
            </form>
        );
    }
}
