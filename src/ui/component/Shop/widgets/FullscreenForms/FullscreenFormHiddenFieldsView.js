import React from 'react';

function FullscreenFormHiddenFieldsView(props) {


    return (
        <div className={"columns"}>
            <div className={"column"}>
                <div className="field">
                    <div className="control">
                        <input className={"input"}
                               type="hidden"
                               placeholder={"print size"}
                               name="size"
                               value={props.printSize}
                        />
                    </div>
                </div>
                <div className="field">
                    <div className="control">
                        <input className={"input"}
                               type="hidden"
                               placeholder={"spec"}
                               name="spec"
                               value={props.printMaterial}
                        />
                    </div>
                </div>
                <div className="field">
                    <div className="control">
                        <input className={"input"}
                               type="hidden"
                               placeholder={"title"}
                               name="title"
                               value={props.title}
                        />
                    </div>
                </div>
                <div className="field">
                    <div className="control">
                        <input className={"input"}
                               type="hidden"
                               placeholder={"img ref"}
                               name="img_ref"
                               value={props.imgPath}
                        />
                    </div>
                </div>
            </div>
        </div>
    );
}

export default FullscreenFormHiddenFieldsView;