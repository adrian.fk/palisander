import React, {Component} from 'react';
import ShopController from "../../../controller/Shop/ShopController";

import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';
import '../Shop.css'

class ShopGalleryCellView extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            id: props.id,
            type: props.type,
            index: props.index,
            title: props.title,
            imgPath: props.imgTnPath
        };

        this.controller = ShopController.getInstance();
    }



    handleCardClick(id, index, type) {
        const cardElem = document.getElementById("card-" + id);
        if (cardElem) {
            cardElem.classList.add("card--enlarge");

            setTimeout(
                () => {
                    cardElem.classList.remove("card--enlarge");
                    this.controller.handleCardClick(id, index, type);
                },
                200
            )
        }
        else {
            this.controller.handleCardClick(id, index, type);
        }
    }

    render() {
        return (
            <section id={"card-" + this.state.id} className={"shop-gallery-cell column is-4"}>
                <div className="card m-3 p-1" onClick={() => this.handleCardClick(this.state.id, this.state.index, this.state.type)} >
                    <LazyLoadImage
                        className={"resize"}
                        src={this.state.imgPath}
                        alt={""}
                    />
                    {/*
                    <div className="card-content p-3">
                        <div className="media">
                            <div className="media-content">
                                <p className={"card-title title is-4"}>{this.state.title}</p>
                            </div>
                        </div>
                    </div>
                    */}
                </div>
            </section>
        );
    }
}

export default ShopGalleryCellView;