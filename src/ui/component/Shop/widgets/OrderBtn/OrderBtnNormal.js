import React from 'react';
import LocalMallIcon from "@material-ui/icons/LocalMall";

function OrderBtnNormal(props) {

    return (
        <button className={"button is-dark is-large is-fullwidth"} type="submit" >
                                    <span className="icon">
                                        <LocalMallIcon />
                                    </span>
            <span>Order</span>
        </button>
    );
}

export default OrderBtnNormal;