import React from 'react';
import DoneIcon from '@material-ui/icons/Done';

function OrderBtnConfirmed(props) {

    return (
        <button className={"button is-primary is-large is-fullwidth"} type="submit" >
                                    <span className="icon">
                                        <DoneIcon />
                                    </span>
            <span>Sent!</span>
        </button>
    );
}

export default OrderBtnConfirmed;