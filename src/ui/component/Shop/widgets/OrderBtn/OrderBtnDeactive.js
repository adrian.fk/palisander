import React from "react";
import LocalMallIcon from "@material-ui/icons/LocalMall";

function OrderBtnDeactive(props) {

    return (
        <button className={"button is-dark is-large is-fullwidth"} type="submit" disabled={true}>
                                    <span className="icon">
                                        <LocalMallIcon />
                                    </span>
            <span>Order</span>
        </button>
    );
}

export default OrderBtnDeactive;