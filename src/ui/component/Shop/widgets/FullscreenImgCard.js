import React from 'react';

import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';

function FullscreenImgCard(props) {


    return (
        <div className="card" >
            <div className="card-image">
                <figure className={"image fullscreen-img"} >
                    <LazyLoadImage
                        src={props.imgPath}
                        placeholderSrc={props.imgTnPath}
                        alt={""}
                        effect={"blur"}
                    />
                </figure>
            </div>
            {/*<div className="card-content p-4">*/}
            {/*    <div className="media">*/}
            {/*        <div className="media-content p-1">*/}
            {/*            <p className={"card-title title is-4"}>{props.title}</p>*/}
            {/*            <p className={"subtitle is-6"}>*/}
            {/*                {props.soldCopies ? "    Sold copies: " + props.soldCopies : ""}*/}
            {/*                {props.price ? "    Price: " + props.price : ""}*/}
            {/*            </p>*/}
            {/*        </div>*/}
            {/*    </div>*/}
            {/*</div>*/}
        </div>
    );
}

export default FullscreenImgCard;