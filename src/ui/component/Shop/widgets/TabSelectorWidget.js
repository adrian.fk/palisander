import React from 'react';
import ShopPageController from "../../../controller/Shop/ShopPageController";

function TabSelectorWidget(props) {

    window.addEventListener("scroll", () => {
        const artworkBtn = document.querySelector(".tab-selector__artwork-btn");
        const merchBtn = document.querySelector(".tab-selector__merch-btn");

        //TODO factor out! Just for trying out..
        const infoBox = document.querySelector(".shop__info-view-widget");

        if (merchBtn && artworkBtn) {
            const pageOffset = window.pageYOffset;
            if (pageOffset < 100) {
                artworkBtn.style.opacity = (100 - window.pageYOffset) / 100;
                merchBtn.style.opacity = (100 - window.pageYOffset) / 100;

            }

            //TODO delete
            if (pageOffset < 150) {
                infoBox.style.opacity = (150 - pageOffset) / 100;
            }
        }
    })

    const handleArtworkBtnClick = () => {

        ShopPageController.handleArtworkClick();
    }

    const handleMerchBtnClick = () => {

        ShopPageController.handleMerchClick();
    }

    return (
        <div className={"shop__tab-selector"} >
            <div className={"shop__tab-selector--content-wrapper"}>
                <div className={"tab-selector__btn tab-selector__artwork-btn"} onClick={() => handleArtworkBtnClick()} >
                    <h3>ARTWORK</h3>
                </div>
                <div className={"tab-selector__btn tab-selector__merch-btn"} onClick={() => handleMerchBtnClick()}>
                    <h3>MERCH</h3>
                </div>
            </div>
        </div>
    );
}

export default TabSelectorWidget;