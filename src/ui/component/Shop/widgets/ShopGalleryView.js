import React, {Component} from 'react';


import ShopFullscreenView from "./ShopFullscreenView";
import ShopGalleryCellView from "./ShopGalleryCellView";

import ShopController from "../../../controller/Shop/ShopController";

import '../Shop.css'
import ShopUserProxy from "../../../../infrastructure/Shop/ShopUserProxy";

class ShopGalleryView extends Component {

    constructor(props, context) {
        super(props, context);

        this.controller = ShopController.getInstance();
        this.controller.bind(this);

        this.controller.promiseGalleryEntries()
            .then((elements) => this.setState({
                renderElements: elements,
            }));

        this.state = {
            renderElements: null,
            fullscreen: false,
            fullscreenComponent: "",
            initialCapture: true,
        };

        ShopUserProxy
            .hasPortedUserView()
            .then(
                () => ShopUserProxy
                        .getPortedUserView()
                        .then(
                            (user) => {
                                this.setState({
                                    fullscreen: true,
                                    fullscreenComponent: <ShopFullscreenView title={user.title} imgPath={user.imgPath} /> ,
                                    initialCapture: false,
                                });
                            }
                        )
                        .catch(errMsg => console.log(errMsg))
            )
            .catch(() => {
                console.log("Did not find any picture to render from gallery");
            });
    }

    displayFullscreen() {
        this.setState({
            fullscreen: true
        });
        window.scrollTo(0, 0);
        //this.controller.scrollSmoothY(window.scrollY, 0);
    }

    exitFullscreen() {
        this.setState({
            fullscreen: false
        });
    }

    setFullscreenComponent(id, title, imgPath, imgTnPath, componentStatement) {
        if (!componentStatement) {
            this.setState({
                fullscreenComponent: <ShopFullscreenView type={"artwork"} title={title} imgPath={imgPath} imgTnPath={imgTnPath} />
            });
        }
        else {
            this.setState({
                fullscreenComponent: componentStatement
            });
        }
    }

    setAndDisplayFullscreenComponent(id, title, imgPath, imgTnPath, componentStatement) {
        this.setFullscreenComponent(id, title, imgPath, componentStatement);
        this.displayFullscreen();
    }


    render() {
        return (
            <div className={"shop-gallery"}>
                <div className={"container"}>
                    <section className={"columns has-3 is-multiline"}>
                        {this.state.renderElements ? this.state.renderElements.map(
                            (element, index) =>
                                < ShopGalleryCellView
                                    key={index}
                                    type={"artwork"}
                                    id={index}
                                    title={element.title}
                                    imgTnPath={element.thumbnail}
                                />
                        ) : ""}
                    </section>
                </div>
                { this.state.fullscreen ? this.state.fullscreenComponent : "" }
            </div>
        );
    }
}

export default ShopGalleryView;
