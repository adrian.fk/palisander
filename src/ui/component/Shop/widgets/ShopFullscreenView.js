import React, {Component} from 'react';
import ShopController from "../../../controller/Shop/ShopController";

import CloseIcon from '@material-ui/icons/Close';


import '../Shop.css'
import FullscreenImgCard from "./FullscreenImgCard";
import ArtworkFullscreenForm from "./FullscreenForms/ArtworkFullscreenForm";
import ArtworkShopController from "../../../controller/Shop/ArtworkShopController";
import MerchFullscreenForm from "./FullscreenForms/MerchFullscreenForm";



class ShopFullscreenView extends Component {

    constructor(props, context) {
        super(props, context);

        this.shopController = ShopController.getInstance();
        this.artworkShopController = ArtworkShopController.getInstance();

        this.artworkShopController.bindView("ShopFullscreenView", this);

        this.state = {
            type: props.type,
            title: props.title,
            imgPath: props.imgPath,
            imgTnPath: props.imgTnPath,

            userName: "",
            userEmail: "",
            userAddress: "",
            printSize: "",
            printMaterial: "",
        };
    }

    ignoreParentListener(e) {
        e.stopPropagation();
    }


    render() {

        return (
            <div className={"shop-gallery-fullscreen"} onClick={() => this.shopController.handleExitFullscreen()}>
            {/*line above represents the transparent background that lays over the shop gallery*/}

                {/* Sets background leaf behind both card and order form */}
                <div className="shop-gallery-fullscreen-wrapper">


                    {/*Exit btn*/}
                    <div className="shop-gallery__exit-button">
                        <CloseIcon />
                    </div>


                    <div className="scroll-icon-container">
                        <div className="scroll-icon-shop">

                        </div>
                    </div>


                    <div className={"container-card--fullscreen is-hidden-mobile"}>
                        <FullscreenImgCard imgPath={this.state.imgPath} imgTnPath={this.state.imgTnPath} title={this.state.title} />
                    </div>
                    <div className={"container-card--fullscreen container-card--fullscreen-mobile is-hidden-tablet"}>
                        <FullscreenImgCard imgPath={this.state.imgPath} imgTnPath={this.state.imgTnPath} title={this.state.title} />
                    </div>


                    <section className={"shop-gallery-form"} onClick={(e) => this.ignoreParentListener(e)} >

                         <h1 className={"shop-gallery-form__title title is-1"}>Contact</h1>


                        { this.state.type === "artwork" ?
                            <ArtworkFullscreenForm
                                title={this.state.title}
                                imgPath={this.state.imgPath}
                                imgTnPath={this.state.imgTnPath}
                            />
                            :
                            <MerchFullscreenForm
                                title={this.state.title}
                                imgPath={this.state.imgPath}
                                imgTnPath={this.state.imgTnPath}
                            />
                        }

                    </section>


                </div>
            </div>
        );
    }

}

export default ShopFullscreenView;