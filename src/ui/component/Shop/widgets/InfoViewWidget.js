

import React from 'react';

function InfoViewWidget(props) {


    return (
        <div className={"shop__info-view-widget"}>
            <p>
                * Artworks might be licensed through an authorized dealership. Click on desired artwork, and fill inn the contact form
                to be set in contact regarding desired artwork.
            </p>
        </div>
    );
}

export default InfoViewWidget;