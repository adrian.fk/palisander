import React, {Component} from 'react';

import Header from "../../Header/Header";
import Footer from "../../Footer/Footer";


import './Shop.css'
import ShopGalleryView from "./widgets/ShopGalleryView";
import InfoViewWidget from "./widgets/InfoViewWidget";
import ShopMerchView from "./widgets/ShopMerchView";
import TabSelectorWidget from "./widgets/TabSelectorWidget";
import ShopPageController from "../../controller/Shop/ShopPageController";


class Shop extends Component {

    constructor(props, context) {
        super(props, context);
        this.className = "shop";

        ShopPageController.bindRootView(this);

        this.state = {
            content: <ShopGalleryView />
        }
    }

    setContentComponent(component) {
        this.setState({
            content: component
        });
    }

    displayArtworkView() {
        const component = < ShopGalleryView />
        this.setContentComponent(component);
    }

    displayMerchView() {
        const component = < ShopMerchView />;
        this.setContentComponent(component);
    }

    render() {
        return (
            <div className={this.className}>
                < Header menuEntry={this.className} />
                < TabSelectorWidget />
                < InfoViewWidget />
                { this.state.content }
                <Footer />
            </div>
        );
    }
}

export default Shop;