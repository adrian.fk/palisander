import React, {Component} from 'react';
import Header from "../../Header/Header";
import Footer from "../../Footer/Footer";
import AboutImageWidget from "./widgets/AboutImageWidget";
import AboutTextWidget from "./widgets/AboutTextWidget";

import "./styles/About.css"


class AboutView extends Component {
    static className = Object.freeze("AboutView");


    render() {
        return (
            <div className={AboutView.className}>
                {/* TODO add active menu entry*/}
                <Header menuEntry={"about"}/>
                <div className="AboutCols--wrapper">
                    <div className="AboutColumns columns">
                        <AboutImageWidget />
                        <AboutTextWidget />
                    </div>
                </div>
                <Footer />
            </div>
        );
    }
}

export default AboutView;