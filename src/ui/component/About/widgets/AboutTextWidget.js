import React, {Component} from 'react';

class AboutTextWidget extends Component {
    static className = Object.freeze("AboutTextWidget");



    render() {
        return (
            <div className={`${AboutTextWidget.className} AboutCol column`}>
                <h3>About Palisanderr</h3>
                <p>
                    Born in 1995, Palisanderr is a Norwegian artist from Oslo. As a son of a gallerist and a stonemason, he was introduced to the art scene at an early age. Growing up, experiencing various galleries around the city, his visual sense and creativeness shaped and formed him artistically from a young age. After years away from the pen and ink, he found back to his artistic curiosity and creativity. Inspired by various artists, from Gunnar S. to Hans Gude, with digital software and melodic house music, Palisanderr emphasises to project his emotions into his art. Palisanderr has sold his artwork to customers in London, New York , Berlin and Oslo. He also designs clothes, digital content, and logos, thus on special request by companies.
                    <br/>
                    <br/>
                    </p>
            </div>
        );
    }
}

export default AboutTextWidget;