import React, {Component} from 'react';
import AboutImage from "../../../../data/assets/image/galleryImg7.jpg"


class AboutImageWidget extends Component {
    static className = Object.freeze("AboutImageWidget");



    render() {
        return (
            <div className={`${AboutImageWidget.className}--wrapper AboutCol column`}>
                <div className={`${AboutImageWidget.className}`}>

                    <img
                        alt={"About"}
                        src={AboutImage}
                    />

                </div>

            </div>
        );
    }
}

export default AboutImageWidget;