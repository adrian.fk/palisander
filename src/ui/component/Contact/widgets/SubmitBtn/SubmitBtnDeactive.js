import React from "react";
import SendIcon from "@material-ui/icons/Send";


function SubmitBtnDeactive(props) {

    return (
        <button className={"button is-dark is-large is-fullwidth"} type="submit" disabled={true}>
                                    <span className="icon">
                                        <SendIcon />
                                    </span>
            <span>Send</span>
        </button>
    );
}

export default SubmitBtnDeactive;