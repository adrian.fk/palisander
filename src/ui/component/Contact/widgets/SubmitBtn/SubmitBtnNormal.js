import React from 'react';
import SendIcon from "@material-ui/icons/Send";


function SubmitBtnNormal(props) {


    return (
        <button className={"button is-dark is-large is-fullwidth"} type="submit" >
                                    <span className="icon">
                                        <SendIcon />
                                    </span>
            <span>Send</span>
        </button>
    );

}

export default SubmitBtnNormal;