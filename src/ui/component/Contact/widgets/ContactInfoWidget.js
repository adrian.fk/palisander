import React, {useState} from 'react';
import ContactController from "../../../controller/Contact/ContactController";

import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';




function ContactInfoWidget(props) {
    const controller = ContactController.getInstance();


    window.addEventListener("scroll",
        () => {
            if (window.pageYOffset > 80) {
                document.getElementById("scroll-icon").classList.add("scroll-icon--hidden");
                document.getElementById("contact-info").classList.add("contact__info--appended-margin");
                if (window.pageYOffset > 120) {
                    document.querySelector(".contact__form-background").classList.add("contact__container-wrapper--tablet--display");
                    document.querySelector(".contact__form-background").classList.remove("contact__container-wrapper--tablet--hide");
                }
            }
            else {
                document.getElementById("scroll-icon").classList.remove("scroll-icon--hidden")
                document.getElementById("contact-info").classList.remove("contact__info--appended-margin");

                if (document.querySelector(".contact__container-wrapper--tablet--display")) {
                    document.querySelector(".contact__form-background").classList.remove("contact__container-wrapper--tablet--display");
                    document.querySelector(".contact__form-background").classList.add("contact__container-wrapper--tablet--hide");
                }
            }
        }
    )


    return (
        <div id={"contact-info"} className={"contact__info"}>

            <div className="imgIll is-hidden-mobile">
                <LazyLoadImage
                    src={"https://palisanderr.netlify.app/static/media/galleryImg48.ec43d8ae.jpg"}
                    alt={""}
                    width={"50%"}
                />
            </div>
            <div className="imgIll is-hidden-tablet">
                <LazyLoadImage
                    src={"https://palisanderr.netlify.app/static/media/galleryImg48.ec43d8ae.jpg"}
                    alt={""}
                    width={"95%"}
                />
            </div>
            <div className="contact-info">
                <p>Pilisanderr</p>
                <p>Peik Otterbeck</p>
                <p>P8beck@gmail.com</p>
            </div>
            <a href="#scroll-icon" id={"scroll-icon"} className="scroll-icon">

            </a>
        </div>
    );
}

export default ContactInfoWidget;