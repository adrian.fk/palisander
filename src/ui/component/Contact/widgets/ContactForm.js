import React, {Component} from 'react';
import ContactController from "../../../controller/Contact/ContactController";


import PersonIcon from "@material-ui/icons/Person";
import EmailIcon from "@material-ui/icons/Email";


import '../Contact.css'
import SubmitBtnDeactive from "./SubmitBtn/SubmitBtnDeactive";
import SubmitBtnNormal from "./SubmitBtn/SubmitBtnNormal";
import SubmitBtnConfirmed from "./SubmitBtn/SubmitBtnConfirmed";



const STATE_ID_FORM_BTN_DEACTIVATED = "deactivated";
const STATE_ID_FORM_BTN_NORMAL = "normal";
const STATE_ID_FORM_BTN_CONFIRMED = "confirmed";


class ContactForm extends Component {

    static STATE_ID_FORM_BTN_DEACTIVATED = STATE_ID_FORM_BTN_DEACTIVATED;
    static STATE_ID_FORM_BTN_NORMAL = STATE_ID_FORM_BTN_NORMAL;
    static STATE_ID_FORM_BTN_CONFIRMED = STATE_ID_FORM_BTN_CONFIRMED;


    constructor(props, context) {
        super(props, context);
        this.state = {
            formBtnState: STATE_ID_FORM_BTN_DEACTIVATED,
            formBtnRender: <SubmitBtnDeactive />,

            userName: "",
            userEmail: "",
            userMsg: "",
        }

        this.controller = ContactController.getInstance();
        this.controller.bind(this);
    }

    setFormRenderState(stateId) {
        setTimeout(
            () => {
                let renderComponent;
                switch (stateId) {
                    case STATE_ID_FORM_BTN_DEACTIVATED:
                        renderComponent = <SubmitBtnDeactive />
                        break;

                    case STATE_ID_FORM_BTN_NORMAL:
                        renderComponent = <SubmitBtnNormal />
                        break;

                    case STATE_ID_FORM_BTN_CONFIRMED:
                        renderComponent = <SubmitBtnConfirmed />
                        break;

                    default:
                        break;
                }

                if (renderComponent) {
                    this.setState({
                        formBtnState: stateId,
                        formBtnRender: renderComponent,
                    });
                }
            },
            200
        )
    }

    checkIfMessageReady() {
        if (this.state.userName !== "" && this.state.userEmail !== "" && this.state.userMsg !== "") {
            this.setFormRenderState(STATE_ID_FORM_BTN_NORMAL);
        }
        else {
            this.setFormRenderState(STATE_ID_FORM_BTN_DEACTIVATED);
        }
    }

    onNameChanged(e) {
        this.setState({
            userName: e.target.value
        });
        this.checkIfMessageReady();
    }

    onEmailChanged(e) {
        this.setState({
            userEmail: e.target.value
        });
        this.checkIfMessageReady();
    }

    onMsgChanged(e) {
        this.setState({
            userMsg: e.target.value
        });
        this.checkIfMessageReady();
    }

    render() {

        return (
            <div className={"contact__form"}>
                <div className="container m-5">

                    <div className="title-container">
                        <h1 className={"contact-title title is-1"}>Contact</h1>
                    </div>

                    <form onSubmit={(e) => this.controller.sendEmail(e)} >
                        <input type="hidden" name="contact_number" />
                        <div className={"columns"}>
                            <div className={"column"}>
                                <div className="field">
                                    <div className="control has-icons-left">
                                        <input className={"input is-medium"}
                                               type="text"
                                               placeholder={"Name"}
                                               name="user_name"
                                               onChange={(e) => this.onNameChanged(e)}
                                        />
                                        <span className="icon is-small is-left">
                                      < PersonIcon />
                                    </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className={"columns"}>
                            <div className={"column"}>
                                <div className="field">
                                    <div className="control has-icons-left">
                                        <input className={"input is-medium"}
                                               type="email"
                                               placeholder={"Email"}
                                               name="user_email"
                                               onChange={(e) => this.onEmailChanged(e)}
                                        />
                                        <span className="icon is-small is-left">
                                      < EmailIcon />
                                    </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className={"columns"}>
                            <div className={"column"}>
                                <div className="field">
                                    <div className="control">
                                        <textarea className={"textarea is-medium    "} name="message" placeholder={"Message"} onChange={(e) => this.onMsgChanged(e)}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {this.state.formBtnRender}
                    </form>
                </div>
            </div>
        );
    }
}

export default ContactForm;