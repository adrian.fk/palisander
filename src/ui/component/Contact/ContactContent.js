import React from 'react';


import './Contact.css';
import ContactForm from "./widgets/ContactForm";
import ContactInfoWidget from "./widgets/ContactInfoWidget";



class ContactContent extends React.Component {

    render() {
        return (

            <div className={"contact__content"}>
                <div className="info-widgets--tablet">
                    <ContactInfoWidget />
                </div>
                <div className="contact__form-background">
                    <div className="contact__container--tablet is-hidden-mobile">
                        <ContactForm />
                    </div>
                    <div className="contact__container--mobile is-hidden-tablet">
                        <ContactForm />
                    </div>
                </div>
            </div>
        );
    }
}

export default ContactContent;