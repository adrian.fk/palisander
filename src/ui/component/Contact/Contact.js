import React, {Component} from 'react';
import Header from "../../Header/Header";
import Footer from "../../Footer/Footer";
import ContactContent from "./ContactContent";


import './Contact.css';
import ContactController from "../../controller/Contact/ContactController";



class Contact extends Component {

    constructor(props, context) {
        super(props, context);
        this.componentName = "contact"

        this.state = {
            currentComponent: < ContactContent />
        }

        this.controller = ContactController.getInstance();
        this.controller.bind(this);
    }

    setRenderComponent(component) {
        this.setState({
            currentComponent: component
        });
    }

    render() {
        return (
            <div className={this.componentName}>
                < Header menuEntry={this.componentName} />

                {this.state.currentComponent}

                < Footer />
            </div>
        );
    }
}

export default Contact;