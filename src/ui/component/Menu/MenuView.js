import React from 'react';
import MenuController from "../../controller/Menu/MenuController";

import './MenuView.css';

function MenuView() {
    const controller = new MenuController();


 return (
   <nav className={"menu"}>
       <section className={"menu__back_btn_container"} onClick={() => controller.handleBackArrowClick()} >
           <section className={"menu__back_btn"} >
               BACK
           </section>
       </section>
       <section className={"menu__menu"} >
           <a href={"./home"}>
               <section className={"menu__item"} onClick={() => controller.handleMenuItemClick("home")}>
                   <h2>HOME</h2>
               </section>
           </a>
           <a href={"./gallery"}>
               <section className={"menu__item"} onClick={() => controller.handleMenuItemClick("gallery")}>
                   <h2>GALLERY</h2>
               </section>
           </a>
           <a href={"./contact"}>
               <section className={"menu__item"} onClick={() => controller.handleMenuItemClick("contact")}>
                   <h2>CONTACT</h2>
               </section>
           </a>
           <a href={"./shop"}>
               <section className={"menu__item"} onClick={() => controller.handleMenuItemClick("shop")}>
                   <h2>SHOP</h2>
               </section>
           </a>
           <a href={"./about"}>
               <section className={"menu__item"} onClick={() => controller.handleMenuItemClick("shop")}>
                   <h2>ABOUT</h2>
               </section>
           </a>
       </section>
   </nav>
 );
}

export default MenuView;