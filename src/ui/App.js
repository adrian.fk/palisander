import React from 'react';
import {
    BrowserRouter,
    Route,
    Switch
} from 'react-router-dom';

import Home from "./component/Home/Home";
import Gallery from "./component/Gallery/Gallery";
import Contact from "./component/Contact/Contact";
import Shop from "./component/Shop/Shop";
import AboutView from "./component/About/AboutView";
import MenuView from "./component/Menu/MenuView";

import AdminPanel from "./admin/AdminPanel";
import AdminMerch from "./admin/component/AdminMerch/AdminMerch";


function App() {
    return (
        <BrowserRouter>
            <Switch>
                <div>
                    < Route path={"/"} exact component={Home} />

                    < Route path={"/home"} exact component={Home} />
                    < Route path={"/gallery"} exact component={Gallery} />
                    < Route path={"/contact"} exact component={Contact} />
                    < Route path={"/shop"} exact component={Shop} />
                    < Route path={"/about"} exact component={AboutView} />
                    < Route path={"/menu"} exact component={MenuView} />


                    < Route path={"/admin"} exact component={AdminPanel} />
                    < Route path={"/admin-gallery"} exact component={AdminPanel}/>
                    < Route path={"/admin-merch"} exact component={AdminMerch}/>

                </div>
            </Switch>
        </BrowserRouter>
    );
}

export default App;