

function MenuController() {

    const handleMenuItemClick = (id) => {
        window.location.assign(`./${id}`);
    }

    const handleBackArrowClick = () => {
        window.history.back();
    }


    return { handleMenuItemClick, handleBackArrowClick };
}

export default MenuController;