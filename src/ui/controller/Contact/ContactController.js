import BaseController from "../../BaseController";
import EmailJS from "emailjs-com";
import ContactForm  from "../../component/Contact/widgets/ContactForm";
import ArtworkRequester from "../../../infrastructure/artwork/ArtworkRequester";


export default
class ContactController extends BaseController {

    static getInstance(initialView) {
        if (!ContactController.INSTANCE) {
            ContactController.INSTANCE = new ContactController(initialView);
        }

        return ContactController.INSTANCE
    }

    promiseIllustration(index) {
        return new Promise(
            resolve => {
                ArtworkRequester()
                    .fetchArtworkByIndex(index)
                    .then((artwork) => resolve(artwork));
            }
        );
    }

    sendEmail(e) {
        e.preventDefault();

        EmailJS.sendForm('service_fnccg87', 'template_v10j7rm', e.target, 'user_gIKUds6CFmqrhXiNrG5Zq')
            .then((result) => {
                console.log(result.text);
                e.target.reset();
                this.view.setFormRenderState(ContactForm.STATE_ID_FORM_BTN_CONFIRMED);
            }, (error) => {
                //e.target.reset();
                console.log(error.text);
            });
    }
}