import BaseController from "../../BaseController";
import EmailJS from "emailjs-com";
import ArtworkRequester from "../../../infrastructure/artwork/ArtworkRequester";
import MerchRequester from "../../../infrastructure/merch/MerchRequester";


export default
class ShopController extends BaseController {

    constructor(initialView) {
        super(initialView);

        this.galleryDataPoolCache = null;
        this.merchDataPoolCache = null;

        ArtworkRequester().fetchArtworkGallery()
            .then((gallery) => this.galleryDataPoolCache = gallery);

        MerchRequester().fetchMerchGallery()
            .then((merchGallery) => this.merchDataPoolCache = merchGallery);
    }

    static getInstance(initialView) {
        if (!ShopController.INSTANCE) {
            ShopController.INSTANCE = new ShopController(initialView);
        }
        return ShopController.INSTANCE;
    }

    handleCardClick(id, index, type) {
        this.prevPos = {
            x: window.scrollX,
            y: window.scrollY
        };
        let target;
        if (type === "artwork") target = this.galleryDataPoolCache[id];
        else if (type === "merch") target = this.merchDataPoolCache[index];
        this.view.setAndDisplayFullscreenComponent(index, target.title, target.imgPath, target.imgTnPath);
    }

    handleExitFullscreen() {
        this.view.exitFullscreen();

        //if (this.prevPos) this.scrollSmoothY(window.scrollY, this.prevPos.y);
        if (this.prevPos) window.scrollTo(this.prevPos.x, this.prevPos.y)
    }

    scrollSmoothY(from, to) {
        const scrollTo = (y) => window.scrollTo(0, y);

        if (from < to) {
            //Were doing a scroll from a to b
            let target = from;
            const runIterator = (target, to) => {
                if (target < to) {
                    target += 40;
                    scrollTo(target);
                    setTimeout(() => runIterator(target, to), 1);
                }
                else {
                    scrollTo(to);
                }
            }
            runIterator(target, to);
        }
        else {
            //Were doing a scroll from b to a
            let target = from;
            const runIterator = (target, to) => {
                if (target > to) {
                    target -= 60;
                    scrollTo(target);
                    setTimeout(() => runIterator(target, to), 1);
                }
                else {
                    scrollTo(to);
                }
            }
            runIterator(target, to);
        }
    }

    handleEnterFullscreen() {
        this.view.displayFullscreen();
    }

    sendOrder(e, callback) {
        e.preventDefault();

        EmailJS.sendForm('service_fnccg87', 'template_zkjo3v5', e.target, 'user_gIKUds6CFmqrhXiNrG5Zq')
            .then((result) => {
                console.log(result.text);
                e.target.reset();
                callback()
            }, (error) => {
                //e.target.reset();
                console.log(error.text);
            });

    }

    promiseGalleryEntries() {
        return new Promise(
            (resolve, reject) => {
                //Check cached data pool
                if (this.galleryDataPoolCache) {
                    resolve(this.galleryDataPoolCache);
                }
                else {
                    ArtworkRequester().fetchArtworkGallery()
                        .then(gallery => {
                            this.galleryDataPoolCache = gallery;
                            resolve(gallery)
                        })
                        .catch(err => reject(err));
                }
            }
        );
    }

    promiseMerchEntries() {
        return new Promise(
            (resolve, reject) => {
                //Check cached data pool
                if (this.merchDataPoolCache) {
                    resolve(this.merchDataPoolCache);
                }
                else {
                    MerchRequester().fetchMerchGallery()
                        .then(gallery => {
                            this.merchDataPoolCache = gallery;
                            resolve(gallery)
                        })
                        .catch(err => reject(err));
                }
            }
        );
    }

}