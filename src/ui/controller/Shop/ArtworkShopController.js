

export default
class ArtworkShopController {


    constructor(initialView = {key: "", view: null}) {
        this.view = new Map();
        if (initialView.key !== "") this.bindView()
    }

    static getInstance() {
        if (!ArtworkShopController.__INSTANCE) {
            ArtworkShopController.__INSTANCE = new ArtworkShopController();
        }
        return ArtworkShopController.__INSTANCE;
    }

    bindView(viewMap = {key: "", view: null}) {
        if (viewMap.key !== "" && viewMap.view !== null) this.view.set(viewMap.key, viewMap.view);
        else console.log("ArtworkShopController.bindView: View could not be set. Missing parameter values");
    }

}