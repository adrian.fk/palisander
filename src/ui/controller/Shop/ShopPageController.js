

const ShopPageController = (() => {

    let rootView;

    const handleArtworkClick = () => {
        if (rootView) rootView.displayArtworkView();
    }

    const handleMerchClick = () => {
        if (rootView) rootView.displayMerchView();
    }

    const bindRootView = (view) => {
        rootView = view;
    }

    return { bindRootView, handleArtworkClick, handleMerchClick }
})();

export default ShopPageController;