import BaseController from "../../BaseController";

export default
class HomeController extends BaseController {

    constructor(numWidgets, homeView) {
        super(homeView);
        this.numWidgets = numWidgets;
        this.poi = 1;
    }

    nav(tarDirection) {
        const next = "next";
        const back = "back";
        const prev = "back";

        if (tarDirection) {
            if (tarDirection.toLowerCase() === back) {
                if (this.poi > 1) {
                    this.poi--;
                }
                else {
                    this.poi = this.numWidgets;
                }
            }
            else if (tarDirection.toLowerCase() === next) {
                if (this.poi < this.numWidgets) {
                    this.poi++;
                }
                else {
                    this.poi = 1;
                }
            }
            this.view.setWidget("widget" + this.poi);
        }
        else return {next, back, prev}
    }

    handleBtnClick() {
        window.location.assign("./shop");
    }
}
