import BaseController from "../../BaseController";
import ArtworkRequester from "../../../infrastructure/artwork/ArtworkRequester";


export default
class GalleryController extends BaseController {

    constructor(initialView) {
        super(initialView);

        this.galleryDataPoolCache = null;

        ArtworkRequester().fetchArtworkGallery()
            .then((gallery) => this.galleryDataPoolCache = gallery);
    }


    static getInstance(initialView) {
        if (!GalleryController.INSTANCE) {
            if (initialView) GalleryController.INSTANCE = new GalleryController(initialView);
            else GalleryController.INSTANCE = new GalleryController();
        }
        return GalleryController.INSTANCE;
    }

    promiseGalleryEntries() {
        return new Promise(
            (resolve, reject) => {
                //Check cached data pool
                if (this.galleryDataPoolCache) {
                    resolve(this.galleryDataPoolCache);
                }
                else {
                    ArtworkRequester().fetchArtworkGallery()
                        .then(gallery => {
                            this.galleryDataPoolCache = gallery;
                            resolve(gallery)
                        })
                        .catch(err => reject(err));
                }
            }
        );
    }

    handleCardClick(id) {
        this.prevPos = {
            x: window.scrollX,
            y: window.scrollY
        };
        const imgCard = this.galleryDataPoolCache[id];
        this.view.setAndDisplayFullscreenComponent(id, imgCard.title, imgCard.imgPath, imgCard.imgTnPath);
    }

    handleExitFullscreen() {
        this.view.exitFullscreen();
        window.scrollTo(this.prevPos.x, this.prevPos.y);
    }

    handleEnterFullscreen() {
        this.view.displayFullscreen();
    }
}