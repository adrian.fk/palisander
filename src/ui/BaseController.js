

export default
class BaseController {

    constructor(initialView) {
        if (initialView) this.view = initialView;
    }

    bind(view, presenter, model) {
        if (view) this.view = view;
        if (presenter) this.presenter = presenter;
        if (model) this.model = model;

        return this;
    }
}