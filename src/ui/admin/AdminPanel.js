import React, {Component} from 'react';
import AdminGallery from "./component/AdminGallery/AdminGallery";
import AdminLoginView from "./component/AdminLogin/AdminLoginView";
import AdminUserProxy from "../../infrastructure/admin/AdminUserProxy";
import AdminLoginController from "./component/AdminLogin/AdminLoginController";

class AdminPanel extends Component {


    constructor(props, context) {
        super(props, context);

        this.loginController = AdminLoginController.getInstance();
        this.loginController.bindView("AdminPanelView", this)

        this.state = {
            renderContent: < AdminGallery />,

            user: AdminUserProxy().getUser(),
        }
    }

    setDisplayUser(displayUser) {
        this.setState({
            user: displayUser
        });
    }

    render() {

        if (!this.state.user) {
            return ( < AdminLoginView /> );
        }
        else
        return (
            <div className={"admin-panel"}>
                {this.state.renderContent}
            </div>
        );
    }
}

export default AdminPanel;