import React from 'react';
import './AdminHeaderMenu.css';
import {Redirect} from "react-router-dom";

const menuItemActive = "header__menu_item--active";
const menuItemInactive = "";

export default class AdminHeaderMenu extends React.Component {

    constructor(props) {
        super(props);


        this.state = {
            redirect: false,
            redirectTargetId: "",

            [props.targetMenuEntry]: true,

        }
    }

    setRedirectDest(destId) {
        this.setState({
            redirect: true,
            redirectTargetId: destId,
        })
    }

    handleClick(id) {
        const prev = this.state.activeMenuItem;

        this.setState({
            activeMenuItem: id,
            [prev]: false,
            [id]: true,
        })

        this.setRedirectDest(id);
    }


    render() {
        if (this.state.redirect) {
            return <Redirect push to={"./admin-" + this.state.redirectTargetId} />
        }
        return (
            <div className={"header__menu"}>
                <div className={`header__menu_item menu__gallery ${this.state.gallery ? menuItemActive : menuItemInactive}`}  onClick={() => this.handleClick("gallery")} >
                    GALLERY
                </div>
                <div className={`header__menu_item menu__merch ${this.state.merch ? menuItemActive : menuItemInactive}`}  onClick={() => this.handleClick("merch")} >
                    MERCH
                </div>

            </div>
        );
    }
}
