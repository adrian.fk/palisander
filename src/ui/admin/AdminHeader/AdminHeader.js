import React from 'react';

import "./AdminHeader.css";

import HeaderLogo from "./brand/HeaderLogo";
import AdminHeaderMenu from "./menu/AdminHeaderMenu";


export default
class AdminHeader extends React.Component {


    render() {
        return (
            <nav id={"admin__header"} className={"admin__header"}>

                <div className={"header__container"}>
                    < HeaderLogo />

                    < AdminHeaderMenu targetMenuEntry={this.props.menuEntry} />

                </div>
            </nav>
        );
    }
}
