import React from 'react';

import headerLogo from "../../../Header/brand/assets/palisanderr_logo.svg";


function HeaderLogo() {

    const handleOnClick = () => {
        window.location.assign("./home");
    }


    return (
        <div className={"header__logo"}>
            <img
                onClick={ () => handleOnClick() }
                src={headerLogo}
                alt={"Logo"}
            />
        </div>
    );
}

export default HeaderLogo;