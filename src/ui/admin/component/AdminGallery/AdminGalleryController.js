import BaseController from "../../../BaseController";
import ArtworkRequester from "../../../../infrastructure/artwork/ArtworkRequester";


export default
class AdminGalleryController extends BaseController {

    constructor(initialView) {
        super(initialView);

        this.galleryDataPoolCache = null;

        ArtworkRequester().fetchArtworkGallery()
            .then((gallery) => this.galleryDataPoolCache = gallery);
    }


    static getInstance(initialView) {
        if (!AdminGalleryController.INSTANCE) {
            if (initialView) AdminGalleryController.INSTANCE = new AdminGalleryController(initialView);
            else AdminGalleryController.INSTANCE = new AdminGalleryController();
        }
        return AdminGalleryController.INSTANCE;
    }

    bind(view, presenter, model) {
        return super.bind(view, presenter, model);
    }

    promiseGalleryEntries() {
        return new Promise(
            (resolve, reject) => {
                //Check cached data pool
                if (this.galleryDataPoolCache) {
                    resolve(this.galleryDataPoolCache);
                }
                else {
                    ArtworkRequester().fetchArtworkGallery()
                        .then(gallery => {
                            this.galleryDataPoolCache = gallery;
                            resolve(gallery)
                        })
                        .catch(err => reject(err));
                }
            }
        );
    }

    promiseGalleryEntry(id) {
        return ArtworkRequester().fetchArtworkById(id);
    }

    attachSnapshotHandler(handlerFunction) {
        ArtworkRequester().onSnapshot(handlerFunction);
    }

    handleCardClick(index, id) {
        this.prevPos = {
            x: window.scrollX,
            y: window.scrollY
        };
        const imgCard = this.galleryDataPoolCache[index];
        this.view.setAndDisplayFullscreenComponent(id, imgCard.title, imgCard.imgPath, imgCard.thumbnail);
    }

    handleExitFullscreen() {
        this.view.exitFullscreen();
        if (this.prevPos && this.prevPos.y) window.scrollTo(this.prevPos.x, this.prevPos.y);
    }

    handleEnterFullscreen() {
        this.view.displayFullscreen();
    }

    handleDeleteImg(imgId) {
        this.galleryDataPoolCache = null;
        return ArtworkRequester().delArtworkById(imgId);
    }

    updateArtwork(id, artwork) {
        return ArtworkRequester().setArtworkById(id, artwork);
    }

    insertArtworkToDbTable(artwork) {
        return ArtworkRequester().addArtwork(artwork);
    }
}