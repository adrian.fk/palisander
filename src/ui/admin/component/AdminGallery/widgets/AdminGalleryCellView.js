import React from 'react';
import AdminGalleryController from "../AdminGalleryController";
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';
import CancelRoundedIcon from '@material-ui/icons/CancelRounded';
import EditRoundedIcon from '@material-ui/icons/EditRounded';

import '../../../styles/AdminGallery.css'
import AdminGalleryView from "./AdminGalleryView";

export default
class AdminGalleryCellView extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            index: props.index,
            id: props.id,
            title: props.title,
            imgTnPath: props.imgTnPath
        }
        this.controller = AdminGalleryController.getInstance();
    }

    onEditClick() {
        const cardElem = document.getElementById("card-" + this.state.id);
        if (cardElem) {
            cardElem.classList.add("card--enlarge");

            setTimeout(
                () => {
                    cardElem.classList.remove("card--enlarge");
                    this.controller.handleCardClick(this.state.index, this.state.id);
                },
                200
            )
        }
        else {
            this.controller.handleCardClick(this.state.id);
        }
    }

    onDeleteBtnClick() {

        this.controller.handleDeleteImg(this.state.id)
            .then(
                (id) => {
                    AdminGalleryView.getInstance()?.displayArtworks();
                }
            );
    }

    render() {
        return (
            <section id={"card-" + this.state.id} className={"admin__gallery-matrix-cell column is-4"}>
                <div className="card m-3 p-2" >
                    <div className={"cancelIcon"} >
                        < CancelRoundedIcon
                            className={"deleteBtn"}
                            onClick={ () => this.onDeleteBtnClick() }
                        />
                    </div>
                    <LazyLoadImage
                        effect={"blur"}
                        wrapperClassName={"img"}
                        alt={""}
                        src={this.state.imgTnPath}
                        placeholderSrc={this.state.imgTnPath}
                        width={"100%"}
                        height={"100%"}
                    />
                    <div className="actions">
                       <button
                           className="button is-primary p-5 edit-btn"
                           onClick={() => this.onEditClick()}
                       >
                           < EditRoundedIcon />
                           <h6>EDIT</h6>
                       </button>
                    </div>
                </div>
            </section>
        );
    }
}