import React from 'react';
import AdminGalleryController from "../AdminGalleryController";
import AdminGalleryCellView from "./AdminGalleryCellView";
import AdminGalleryFullscreenView from "./AdminGalleryFullscreenView";
import AdminGalleryAddView from "./AdminGalleryAddView";
import AddRoundedIcon from '@material-ui/icons/AddRounded';


export default
class AdminGalleryView extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.controller = AdminGalleryController.getInstance();
        this.controller.bind(this);


        this.state = {
            renderElements: null,
            fullscreen: false,
            fullscreenComponent: null
        }

        this.controller.attachSnapshotHandler(
            (elements) => {
                this.setState({renderElements: []})
                this.setState({
                    renderElements: [...elements],
                })
            });

        AdminGalleryView.__instance = this;
    }

    /**
     * @return {AdminGalleryView}
     */
    static getInstance() {
        return AdminGalleryView.__instance;
    }

    displayFullscreen() {
        this.setState({
            fullscreen: true
        });
        window.scrollTo(0, 0);
    }

    exitFullscreen() {
        this.setState({
            fullscreen: false
        });
    }

    setFullscreenComponent(id, title, imgPath, imgTnPath) {
        this.setState({
            fullscreenComponent: <AdminGalleryFullscreenView id={id} title={title} imgPath={imgPath} imgTnPath={imgTnPath} />
        });
    }

    setAndDisplayFullscreenComponent(id, title, imgPath, imgTnPath) {

        this.setFullscreenComponent(id, title, imgPath, imgTnPath);
        this.displayFullscreen();
    }

    setAndDisplayAddView() {
        this.setState({
            fullscreenComponent: < AdminGalleryAddView />
        });
        this.displayFullscreen();
    }

    addBtnClick() {
        //set fullscreen component to add component and display fullscreen
        this.setAndDisplayAddView();
    }

    displayArtworks() {
        this.controller.promiseGalleryEntries().then(
            (elements) => this.setState({renderElements: [...elements]})
        );
    }

    render() {
        return (
            <div className={"admin__gallery-matrix"}>
                <div className="add-section">
                    <AddRoundedIcon className={"add-icon"} onClick={() => this.addBtnClick()}/>
                </div>
                <div className={"container"}>
                    <section className={"matrix columns has-3 is-multiline"}>
                        {this.state.renderElements ? this.state.renderElements.map(
                            (element, index) =>
                                < AdminGalleryCellView
                                    key={index}
                                    index={index}
                                    id={element.id}
                                    title={element.title}
                                    imgTnPath={element.thumbnail}
                                />
                        ) : ""}
                    </section>
                </div>
                { this.state.fullscreen ? this.state.fullscreenComponent : "" }
            </div>
        );
    }

}
