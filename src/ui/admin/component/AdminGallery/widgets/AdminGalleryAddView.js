import React, {useState} from 'react';
import AdminGalleryController from "../AdminGalleryController";
import  { Redirect } from 'react-router-dom'
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';
import ArtworkInputViewAdapter from "../adapter/ArtworkInputViewAdapter";


function AdminGalleryFullscreenView(props) {

    const [state, setState] = useState({
        title: props.title,
        imgPath: props.imgPath,
        imgTnImg: props.imgTnPath,
        redirectToShop: false,
    });


    const controller = AdminGalleryController.getInstance();


    const ignoreParentListener = (e) => {
        e.stopPropagation();
    }

    const imgPathChanged = (e) => {
        setState({...state, imgPath: e.target.value})
    }

    const imgTnPathChanged = (e) => {
        //Do nothing
    }

    const imgTitleChanged = (e) => {
        //Do nothing
        setState({...state, title: e.target.value})
    }

    const displaySaveSuccessPrompt = (savedArtwork) => {
        alert("Artwork saved successfully");
    }

    const displaySaveUnsuccessfulPrompt = (error) => {
        alert("Could not add image..");
    }

    const submitGalleryEntry = () => {
        const inputTitle = document.getElementById("admin-gallery-add__title").value;
        const inputImgPath = document.getElementById("admin-gallery-add__imgPath").value;
        const inputImgTnPath = document.getElementById("admin-gallery-add__thumbnail").value
        const inputPrice = document.getElementById("admin-gallery-add__price").value;
        const inputSoldCopies = document.getElementById("admin-gallery-add__sold-copies").value;
        const inputDescription = document.getElementById("admin-gallery-add__description").value;

        controller.insertArtworkToDbTable(
            ArtworkInputViewAdapter.mapInputToArtwork(
                inputTitle,
                inputImgPath,
                inputImgTnPath,
                inputPrice,
                inputSoldCopies,
                inputDescription
            )
        )
        .then((artwork) => displaySaveSuccessPrompt(artwork))
        .catch((err) => displaySaveUnsuccessfulPrompt(err));
    }


    if (state.redirectToShop) {
        return (
            <Redirect push to='./shop' />
        )
    }
    else
    return (
        <div className={"admin-gallery_matrix_fullscreen"} onClick={() => controller.handleExitFullscreen()}>
            <div className={"admin-container--fullscreen"}>

                <h1>
                    {state.title ?
                        state.title
                        : ""
                    }
                </h1>

                {state.imgPath ?
                    < LazyLoadImage
                        effect={"blur"}
                        src={state.imgPath}
                        placeholderSrc={state.imgPath}
                        alt={""}
                    />

                    : ""
                }

                <div className="admin-gallery-add m-3 p-3" onClick={event => ignoreParentListener(event)}>
                    <div className="admin-gallery-add__title m-3">
                        <input
                            id={"admin-gallery-add__title"}
                            className={"input is-3"}
                            placeholder={"Title"}
                            onChange={e => imgTitleChanged(e)}
                        />
                    </div>
                    <div className="admin-gallery-add__imgPath m-3">
                        <input
                            id={"admin-gallery-add__imgPath"}
                            className={"input is-3"}
                            placeholder={"Image URL"}
                            onChange={e => imgPathChanged(e)}
                        />
                    </div>
                    <div className="admin-gallery-add__thumbnail m-3">
                        <input
                            id={"admin-gallery-add__thumbnail"}
                            className={"input is-3"}
                            placeholder={"Thumbnail URL"}
                            onChange={e => imgTnPathChanged(e)}
                        />
                    </div>
                    <div className="admin-gallery-add__price m-3">
                        <input
                            id={"admin-gallery-add__price"}
                            className={"input is-3"}
                            placeholder={"Price"}
                        />
                    </div>
                    <div className="admin-gallery-add__sold-copies m-3">
                        <input
                            id={"admin-gallery-add__sold-copies"}
                            className={"input is-3"}
                            placeholder={"Sold copies"}
                        />
                    </div>
                    <div className="admin-gallery-add__description m-3">
                        <textarea
                            id={"admin-gallery-add__description"}
                            className={"textarea is-3"}
                            placeholder={"Description"}
                        />
                    </div>

                    <div className="admin-gallery-add__submit-btn m-3">
                        <button
                            id={"admin-gallery-add__submit-btn"}
                            className={"button is-dark"}
                            onClick={() => submitGalleryEntry()}
                        >
                            Submit
                        </button>
                    </div>
                </div>


            </div>
        </div>
    );
}

export default AdminGalleryFullscreenView;
