import React, {useEffect, useState} from 'react';
import AdminGalleryController from "../AdminGalleryController";
import  { Redirect } from 'react-router-dom'
import { LazyLoadImage } from 'react-lazy-load-image-component';
import ArtworkInputViewAdapter from "../adapter/ArtworkInputViewAdapter";

import 'react-lazy-load-image-component/src/effects/blur.css';


function AdminGalleryFullscreenView(props) {

    const [state, setState] = useState({
        id: props.id,
        title: props.title,
        imgPath: props.imgPath,
        imgTnPath: props.imgTnPath || "",
        description: "",
        soldCopies: "",
        price: "",

        redirectToShop: false,
    });


    const controller = AdminGalleryController.getInstance();

    useEffect(
        () => {
            controller
                .promiseGalleryEntry(props.id)
                .then(
                    (artwork) => {
                        setState({
                            ...state,
                            description: artwork.description,
                            soldCopies: artwork.soldCopies,
                            price: artwork.price
                        })
                    }
                )
                .catch(() => {});

        }, []
    );



    const ignoreParentListener = (e) => {
        e.stopPropagation();
    }

    const imgPathChanged = (e) => {
        setState({...state, imgPath: e.target.value})
    }

    const imgTnPathChanged = (e) => {
        setState({...state, imgTnPath: e.target.value})

    }

    const imgTitleChanged = (e) => {
        setState({...state, title: e.target.value})
    }

    const onPriceChange = (e) => {
        setState({
            ...state,
            price: e.target.value
        });
    }

    const onSoldCopiesChange = (e) => {
        setState({
            ...state,
            soldCopies: e.target.value
        });
    }

    const onDescriptionChange = (e) => {
        setState({
            ...state,
            description: e.target.value
        });
    }

    const displaySaveSuccessPrompt = (savedArtwork) => {
        alert("Artwork updated successfully");
    }

    const displaySaveUnsuccessfulPrompt = (error) => {
        alert("Could not update image..");
    }

    const checkInputFields = (artworkObj) => {
        const o = {
            ...artworkObj
        }

        if (artworkObj.title === "") o.title = state.title;
        if (artworkObj.imgPath === "") o.imgPath = state.imgPath;
        if (artworkObj.thumbnail === "") o.thumbnail = state.imgTnPath;
        if (artworkObj.description === "") o.description = state.description;
        if (artworkObj.soldCopies === "") o.soldCopies = state.soldCopies;
        if (artworkObj.price === "") o.price = state.price;


        return o;
    }

    const submitGalleryEntry = () => {
        const inputTitle = document.getElementById("admin-gallery-add__title").value;
        const inputImgPath = document.getElementById("admin-gallery-add__imgPath").value;
        const inputImgTnPath = document.getElementById("admin-gallery-add__thumbnail").value
        const inputPrice = document.getElementById("admin-gallery-add__price").value;
        const inputSoldCopies = document.getElementById("admin-gallery-add__sold-copies").value;
        const inputDescription = document.getElementById("admin-gallery-add__description").value;

        let artworkObj = ArtworkInputViewAdapter.mapInputToArtwork(
            inputTitle,
            inputImgPath,
            inputImgTnPath,
            inputPrice,
            inputSoldCopies,
            inputDescription,
            state.id
        );

        artworkObj = checkInputFields(artworkObj);

        controller.updateArtwork(
            state.id,
            artworkObj
        )
            .then((artwork) => displaySaveSuccessPrompt(artwork))
            .catch((err) => displaySaveUnsuccessfulPrompt(err));
    }

    if (state.redirectToShop) {
        return (
            <Redirect push to='./shop' />
        )
    }
    else
    return (
        <div className={"admin-gallery_matrix_fullscreen"} onClick={() => controller.handleExitFullscreen()}>
            <div className={"admin-container--fullscreen"}>
                <section className={"columns"}>


                    <section className={"gallery_matrix_fullscreen_cell column"}>
                        <figure className={"image fullscreen-img"} >
                            <LazyLoadImage
                                effect={"blur"}
                                src={state.imgPath}
                                placeholderSrc={state.imgPath}
                                alt={""}
                            />
                        </figure>
                    </section>
                </section>


                <h1>
                    {state.title ?
                        state.title
                        : ""
                    }
                </h1>


                <div className="admin-gallery-add m-3 p-3" onClick={event => ignoreParentListener(event)}>
                    <div className="admin-gallery-add__title m-3">
                        <input
                            id={"admin-gallery-add__title"}
                            className={"input is-3"}
                            placeholder={"Title"}
                            onChange={e => imgTitleChanged(e)}
                            value={state.title}
                        />
                    </div>
                    <div className="admin-gallery-add__imgPath m-3">
                        <input
                            id={"admin-gallery-add__imgPath"}
                            className={"input is-3"}
                            placeholder={"Image URL"}
                            onChange={e => imgPathChanged(e)}
                            value={state.imgPath}
                        />
                    </div>
                    <div className="admin-gallery-add__thumbnail m-3">
                        <input
                            id={"admin-gallery-add__thumbnail"}
                            className={"input is-3"}
                            placeholder={"Thumbnail URL"}
                            onChange={e => imgTnPathChanged(e)}
                            value={state.imgTnPath}
                        />
                    </div>
                    <div className="admin-gallery-add__price m-3">
                        <input
                            id={"admin-gallery-add__price"}
                            className={"input is-3"}
                            placeholder={"Price"}
                            onChange={event => onPriceChange(event)}
                            value={state.price}
                        />
                    </div>
                    <div className="admin-gallery-add__sold-copies m-3">
                        <input
                            id={"admin-gallery-add__sold-copies"}
                            className={"input is-3"}
                            placeholder={"Sold copies"}
                            onChange={e => onSoldCopiesChange(e)}
                            value={state.soldCopies}
                        />
                    </div>
                    <div className="admin-gallery-add__description m-3">
                        <textarea
                            id={"admin-gallery-add__description"}
                            className={"textarea is-3"}
                            placeholder={"Description"}
                            onChange={e => onDescriptionChange(e)}
                            value={state.description}
                        />
                    </div>

                    <div className="admin-gallery-add__submit-btn m-3">
                        <button
                            id={"admin-gallery-add__submit-btn"}
                            className={"button is-dark"}
                            onClick={() => submitGalleryEntry()}
                        >
                            Submit
                        </button>
                    </div>
                </div>

            </div>
        </div>
    );
}

export default AdminGalleryFullscreenView;
