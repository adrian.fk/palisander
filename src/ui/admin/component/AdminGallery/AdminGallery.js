import React from 'react';
import AdminGalleryView from "./widgets/AdminGalleryView";
import AdminHeader from "../../AdminHeader/AdminHeader";


export default
class AdminGallery extends React.Component{

    constructor(props) {
        super(props);
        this.className = "admin__gallery"
        this.state = {
            content: < AdminGalleryView />
        }
    }

    setContentComponent(component) {
        this.setState({
            content: component
        });
    }

    render() {
        return (
            <div className={this.className}>
                < AdminHeader menuEntry={"gallery"} />
                {this.state.content}
            </div>
        );
    }
}