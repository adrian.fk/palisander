import React from 'react';
import AdminMerchView from "./widgets/AdminMerchView";
import AdminHeader from "../../AdminHeader/AdminHeader";
import AdminUserProxy from "../../../../infrastructure/admin/AdminUserProxy";
import {Redirect} from "react-router-dom";


export default
class AdminMerch extends React.Component{

    constructor(props) {
        super(props);
        this.className = "admin__merch"
        this.state = {
            content: < AdminMerchView />
        }
        this.user = AdminUserProxy().getUser();
    }

    setContentComponent(component) {
        this.setState({
            content: component
        });
    }

    render() {
        if (!this.user) {
            return (< Redirect to={"./admin"} />)
        }
        return (
            <div className={this.className}>
                < AdminHeader menuEntry={"merch"} />
                {this.state.content}
            </div>
        );
    }
}