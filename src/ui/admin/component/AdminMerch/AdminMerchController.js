import BaseController from "../../../BaseController";
import MerchRequester from "../../../../infrastructure/merch/MerchRequester";


export default
class AdminMerchController extends BaseController {

    constructor(initialView) {
        super(initialView);

        this.galleryDataPoolCache = null;

        MerchRequester().fetchMerchGallery()
            .then((gallery) => this.galleryDataPoolCache = gallery);
    }


    static getInstance(initialView) {
        if (!AdminMerchController.INSTANCE) {
            if (initialView) AdminMerchController.INSTANCE = new AdminMerchController(initialView);
            else AdminMerchController.INSTANCE = new AdminMerchController();
        }
        return AdminMerchController.INSTANCE;
    }

    bind(view, presenter, model) {
        return super.bind(view, presenter, model);
    }

    promiseMerchEntries() {
        return new Promise(
            (resolve, reject) => {
                //Check cached data pool
                if (this.galleryDataPoolCache) {
                    resolve(this.galleryDataPoolCache);
                }
                else {
                    MerchRequester().fetchMerchGallery()
                        .then(gallery => {
                            this.galleryDataPoolCache = gallery;
                            resolve(gallery)
                        })
                        .catch(err => reject(err));
                }
            }
        );
    }

    promiseGalleryEntry(id) {
        return MerchRequester().fetchMerchById(id);
    }

    attachSnapshotHandler(handlerFunction) {
        MerchRequester().onSnapshot(handlerFunction);
    }

    handleCardClick(index, id) {
        this.prevPos = {
            x: window.scrollX,
            y: window.scrollY
        };
        const imgCard = this.galleryDataPoolCache[index];
        this.view.setAndDisplayFullscreenComponent(id, imgCard.title, imgCard.imgPath, imgCard.thumbnail);
    }

    handleExitFullscreen() {
        this.view.exitFullscreen();
        if (this.prevPos && this.prevPos.y) window.scrollTo(this.prevPos.x, this.prevPos.y);
    }

    handleEnterFullscreen() {
        this.view.displayFullscreen();
    }

    handleDeleteImg(imgId) {
        return MerchRequester().delById(imgId);
    }

    updateArtwork(id, artwork) {
        return MerchRequester().setById(id, artwork);
    }

    insertArtworkToDbTable(stateBundle) {
        return MerchRequester().addMerch(stateBundle);
    }
}