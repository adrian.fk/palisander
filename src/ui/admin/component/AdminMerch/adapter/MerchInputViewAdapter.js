

const MerchInputViewAdapter = (() => {

    const mapInputToArtwork = (
        inputTitle,
        inputImgPath,
        inputTnImgPath,
        inputPrice,
        inputSoldCopies,
        inputDescription,
        inputID ) => {

        const out = {
            title: inputTitle,
            imgPath: inputImgPath,
            thumbnail: inputTnImgPath,
            price: inputPrice,
            soldCopies: inputSoldCopies,
            description: inputDescription,
        }

        if (inputID) {
            return { ...out, id: inputID }
        }

        return out;
    }

    return { mapInputToArtwork }

}) ();

export default MerchInputViewAdapter;
