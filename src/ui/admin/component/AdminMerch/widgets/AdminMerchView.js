import React from 'react';
import AdminMerchController from "../AdminMerchController";
import AdminMerchCellView from "./AdminMerchCellView";
import AdminMerchFullscreenView from "./AdminMerchFullscreenView";
import AdminGalleryAddView from "./AdminMerchAddView";
import AddRoundedIcon from '@material-ui/icons/AddRounded';


export default
class AdminMerchView extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.controller = AdminMerchController.getInstance();
        this.controller.bind(this);


        this.state = {
            renderElements: null,
            fullscreen: false,
            fullscreenComponent: null
        }

        this.controller.attachSnapshotHandler(
            (elements) => {
                this.clearRenderElements();
                this.setState({
                    renderElements: [ ...elements ],
                })
            });
    }

    clearRenderElements() {
        this.setState({
            renderElements: []
        })
    }

    displayFullscreen() {
        this.setState({
            fullscreen: true
        });
        window.scrollTo(0, 0);
    }

    exitFullscreen() {
        this.setState({
            fullscreen: false
        });
    }

    setFullscreenComponent(id, title, imgPath, imgTnPath) {
        this.setState({
            fullscreenComponent: <AdminMerchFullscreenView id={id} title={title} imgPath={imgPath} imgTnPath={imgTnPath} />
        });
    }

    setAndDisplayFullscreenComponent(id, title, imgPath, imgTnPath) {

        this.setFullscreenComponent(id, title, imgPath, imgTnPath);
        this.displayFullscreen();
    }

    setAndDisplayAddView() {
        this.setState({
            fullscreenComponent: < AdminGalleryAddView />
        });
        this.displayFullscreen();
    }

    addBtnClick() {
        //set fullscreen component to add component and display fullscreen
        this.setAndDisplayAddView();
    }

    render() {
        return (
            <div className={"admin__gallery-matrix"}>
                <div className="add-section">
                    <AddRoundedIcon className={"add-icon"} onClick={() => this.addBtnClick()}/>
                </div>
                <div className={"container"}>
                    <section className={"matrix columns has-3 is-multiline"}>
                        {this.state.renderElements ? this.state.renderElements.map(
                            (element, index) =>
                                < AdminMerchCellView
                                    key={index}
                                    index={index}
                                    id={element.id}
                                    title={element.title}
                                    imgTnPath={element.thumbnail}
                                />
                        ) : ""}
                    </section>
                </div>
                { this.state.fullscreen ? this.state.fullscreenComponent : "" }
            </div>
        );
    }

}
