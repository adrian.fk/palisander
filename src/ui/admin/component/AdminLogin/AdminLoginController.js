import AdminUserManager from "../../../../infrastructure/admin/AdminUserManager";
import AdminUserAdapter from "../../../../domain/adapter/AdminUserAdapter";
import AdminUserVerificator from "../../../../infrastructure/admin/AdminUserVerification/AdminUserVerificator";
import AdminUserProxy from "../../../../infrastructure/admin/AdminUserProxy";


export default
class AdminLoginController {

    constructor() {
        this.view = new Map();

    }

    static getInstance() {
        if (!AdminLoginController.INSTANCE) {
            AdminLoginController.INSTANCE = new AdminLoginController();
        }
        return AdminLoginController.INSTANCE;
    }

    resolveError(err) {

        console.log(err.error);

        switch (err.type) {

        }
    }

    signIn() {

        AdminUserManager().loginWithPopup()
            .then(
                (res) => {
                    const user = AdminUserAdapter().mapToAdminUser(res.user.uid, res.user.email, res.user.displayName);
                    AdminUserVerificator().verifyUser(user)
                        .then(
                            resUser => {
                                AdminUserProxy().setUser(user);
                                this.view.get("AdminPanelView").setDisplayUser(resUser);
                            }
                        )
                        .catch((err => this.resolveError(err)));
                }
            );
    }

    bindView(viewName, view) {
        this.view.set(viewName, view);
    }


}