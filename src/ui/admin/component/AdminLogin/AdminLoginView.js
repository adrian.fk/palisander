import React from "react";
import HeaderLogo from "../../AdminHeader/brand/HeaderLogo";



import '../../styles/AdminLogin.css'
import AdminLoginController from "./AdminLoginController";

class AdminLoginView extends React.Component {

    constructor(props, context) {
        super(props, context);

        this.controller = AdminLoginController.getInstance();
        this.controller.bindView("AdminLoginView", this);
    }



    signIn() {
        this.controller.signIn();
    }

    render() {
        return (
            <div className={"admin__login"}>

                <div className="login__logo-wrapper">
                    < HeaderLogo/>
                </div>

                <button
                    className="button is-dark login__login-btn p-5"
                    onClick={() => this.signIn()}
                >
                    LOGIN
                </button>
            </div>
        );
    }
}

export default AdminLoginView;