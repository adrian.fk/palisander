import React from 'react';
import InstagramIcon from '@material-ui/icons/Instagram';
import './HeaderSocialMedia.css'

function HeaderSocialMedia() {
    function onClick() {
        window.location.assign("https://www.instagram.com/palisanderr/");
    }

    return (
        <div className={"header__social_media"} onClick={() => onClick()}>
           <InstagramIcon className={"ig_icon"} />
        </div>
    );
}

export default HeaderSocialMedia;