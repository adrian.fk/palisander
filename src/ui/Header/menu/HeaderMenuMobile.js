import React from 'react';
import './HeaderMenu.css';
import MenuRoundedIcon from '@material-ui/icons/MenuRounded';

export default class HeaderMenu extends React.Component {

    handleClick() {
        window.location.assign(`./menu`);
    }


    render() {
        return (
            <div className={"header__menu--mobile"}>
                <MenuRoundedIcon className={"header__menu--mobile__icon"} onClick={() => this.handleClick()} />
            </div>
        );
    }
}
