import React from 'react';
import './HeaderMenu.css';

const menuItemActive = "header__menu_item--active";
const menuItemInactive = "";

export default class HeaderMenu extends React.Component {

    constructor(props) {
        super(props);


        this.state = {
            [props.targetMenuEntry]: true
        }

        console.log(this.props.targetMenuEntry);
    }


    handleClick(id) {
        window.location.assign(`./${id}`);

        const prev = this.state.activeMenuItem;

        this.setState({
            activeMenuItem: id,
            [prev]: false,
            [id]: true,
        })
    }


    render() {
        return (
            <div className={"header__menu"}>
                <div className={`header__menu_item menu__home ${this.state.home ? menuItemActive : menuItemInactive}`} onClick={() => this.handleClick("home")} >
                    HOME
                </div>
                <a href={"./gallery"}>
                    <div className={`header__menu_item menu__gallery ${this.state.gallery ? menuItemActive : menuItemInactive}`}  onClick={() => this.handleClick("gallery")} >
                        GALLERY
                    </div>
                </a>
                <a href={"./contact"}>
                    <div className={`header__menu_item menu__contact ${this.state.contact ? menuItemActive : menuItemInactive}`}  onClick={() => this.handleClick("contact")} >
                        CONTACT
                    </div>
                </a>
                <a href={"./shop"}>
                    <div className={`header__menu_item menu__shop ${this.state.shop ? menuItemActive : menuItemInactive}`}  onClick={() => this.handleClick("shop")} >
                        SHOP
                    </div>
                </a>
                <a href={"./about"}>
                    <div className={`header__menu_item menu__about ${this.state.about ? menuItemActive : menuItemInactive}`}  onClick={() => this.handleClick("about")} >
                        ABOUT
                    </div>
                </a>
            </div>
        );
    }
}
