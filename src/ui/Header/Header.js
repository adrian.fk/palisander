import React from 'react';

import "./Header.css";

import HeaderLogo from "./brand/HeaderLogo";
import HeaderMenu from "./menu/HeaderMenu";
import HeaderMenuMobile from "./menu/HeaderMenuMobile";
import HeaderSocialMedia from "./references/HeaderSocialMedia";


export default
class Header extends React.Component {

    constructor(props, context) {
        super(props, context);
        window.addEventListener("scroll",
            () => {
                if (window.pageYOffset > 80) {
                    document.getElementById("app__header").classList.add("fixedHeader");
                }
                else {
                    document.getElementById("app__header").classList.remove("fixedHeader")
                }
            }
        )
    }

    render() {
        return (
            <nav id={"app__header"} className={"app__header"}>

                <div className={"header__container is-hidden-mobile"}>
                    < HeaderLogo />

                    < HeaderMenu targetMenuEntry={this.props.menuEntry} />

                    < HeaderSocialMedia />
                </div>
                <div className={"header__container is-hidden-tablet"}>
                    < HeaderLogo />

                    <div className={"header__container__nav"} >
                        < HeaderMenuMobile />
                        < HeaderSocialMedia />
                    </div>
                </div>
            </nav>
        );
    }
}
