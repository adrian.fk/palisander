import React from 'react';


import FooterConfig from "../../configuration/FooterConfig";

import './Footer.css'


function Footer() {
    const config = FooterConfig.getConfig()


    return (
        <div className={"footer"}>
            <div className="copyright-statement-container">
                <div className="column">
                    <div className="copyright-statement">
                        <p>{config.copyrightStatement}</p>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Footer;